﻿namespace EFilm.Data.S3Obj
{
    public class S3RquestDirectory
    {
        public string BucketName { get; set; } = null!;
        public string Directory { get; set; } = null!;
        public string Prefix { get; set; } = null!;
    }
}
