﻿namespace EFilm.Data.S3Obj
{
    public class S3RequestData
    {
        public string Name { get; set; } = null!;
        public MemoryStream? InputStream { get; set; } = null!;
        public string BucketName { get; set; } = null!;
    }
}
