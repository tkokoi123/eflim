﻿namespace EFilm.Data.Dtos
{
    public class CacheServiceDto
    {
        public string Token { get; set; }
        public DateTime ExpriedTime { get; set; }

        public CacheServiceDto(string token, DateTime expriedTime)
        {
            Token = token;
            ExpriedTime = expriedTime;
        }
    }
}
