﻿using EFilm.Data.Entities;

namespace EFilm.Data.Dtos.Requests
{
    public class FilterMovieRequestModel
    {
        public Guid GenreId { get; set; }
        public Guid CountryId { get; set; }
        public TypeMovie TypeMovie { get; set; } = TypeMovie.None;
        public TypeSubtitle TypeSubtitle { get; set; } = TypeSubtitle.None;
        public short ReleaseYear { get; set; } = -1;
    }
}
