﻿namespace EFilm.Data.Dtos.Requests
{
    public class GetUrlRequestModel
    {
        public string Filename { get; set; } = string.Empty;
    }
}
