﻿using System.ComponentModel.DataAnnotations;

namespace EFilm.Data.Dtos.Requests
{
    public class UserLoginRequestModel
    {
        [Required(ErrorMessage = "Username is required")]
        [MaxLength(20, ErrorMessage = "Username max length is 20")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [MaxLength(50, ErrorMessage = "Password max length is 50")]
        public string Password { get; set; }

        public UserLoginRequestModel(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public UserLoginRequestModel()
        {
        }
    }
}
