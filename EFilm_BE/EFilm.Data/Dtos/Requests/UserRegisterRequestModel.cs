﻿using System.ComponentModel.DataAnnotations;

namespace EFilm.Data.Dtos.Requests
{
    public class UserRegisterRequestModel
    {
        [Required(ErrorMessage = "Please input this field!")]
        [MaxLength(25, ErrorMessage = "Max size of field is 25")]
        public string Username { get; set; }

        [Required]
        [MaxLength(50)]
        public string Password { get; set; }

        [MaxLength(255)]
        [Required]
        public string Email { get; set; }

        [Required]
        public bool Gender { get; set; }

        [Required]
        [MaxLength(255)]
        public string Fullname { get; set; }

        public UserRegisterRequestModel(string username, string password, string email, bool gender, string fullname)
        {
            Username = username;
            Password = password;
            Email = email;
            Gender = gender;
            Fullname = fullname;
        }

        public UserRegisterRequestModel()
        {
        }

    }
}
