﻿namespace EFilm.Data.Dtos.Requests
{
    public class UserRefreshTokenRequestModel
    {
        public string RefreshToken { get; set; }
        public string AccessToken { get; set; }

        public UserRefreshTokenRequestModel(string refreshToken, string accessToken)
        {
            RefreshToken = refreshToken;
            AccessToken = accessToken;
        }
    }
}
