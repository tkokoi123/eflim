﻿using System.ComponentModel.DataAnnotations;

namespace EFilm.Data.Dtos.Requests
{
    public class NewMovieRequestModel
    {
        [MinLength(1, ErrorMessage = "Min length is 1"), MaxLength(255, ErrorMessage = "Maxlength is 255")]
        public string Title { get; set; }

        [MinLength(1, ErrorMessage = "Min length is 1")]
        public string Overview { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Duration { get; set; }
        public string CountryId { get; set; }
        public string Thumbnail { get; set; }
        public int MovieType { get; set; }
        public bool IsHot { get; set; }

        public NewMovieRequestModel()
        {
            
        }

        public NewMovieRequestModel(string title, string overview, DateTime releaseDate, string duration, string countryId, string thumbnail, int movieType, bool isHot)
        {
            Title = title;
            Overview = overview;
            ReleaseDate = releaseDate;
            Duration = duration;
            CountryId = countryId;
            Thumbnail = thumbnail;
            MovieType = movieType;
            IsHot = isHot;
        }
    }
}
