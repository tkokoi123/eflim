﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFilm.Data.Dtos.Responses
{
    public class CommentResponseModel
    {
        public Guid CommentId { get; set; }
        public Guid UserId { get; set; }
        public Guid MovieId { get; set; }
        public string Content { get; set; }
    }
}
