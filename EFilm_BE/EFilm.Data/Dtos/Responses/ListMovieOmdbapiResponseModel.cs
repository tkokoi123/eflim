﻿namespace EFilm.Data.Dtos.Responses
{
    public class ListMovieOmdbapiResponseModel
    {
        public string Title { get; set; }
        public string Year { get; set; }
        public string ImdbId { get; set; }
        public TypeEnum Type { get; set; }
        public Uri Poster { get; set; }
    }
}
