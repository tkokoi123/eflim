﻿namespace EFilm.Data.Dtos.Responses
{
    public class AdminMovieResponseModel
    {
        public Guid MovieId { get; set; }
        public string Title { get; set; }
        public string ThumbnailUrl { set; get; }
        public string MovieType { get; set; }

        public AdminMovieResponseModel()
        {
            
        }

        public AdminMovieResponseModel(Guid movieId, string title, string thumbnailUrl, string movieType)
        {
            MovieId = movieId;
            Title = title;
            ThumbnailUrl = thumbnailUrl;
            MovieType = movieType;
        }
    }
}
