﻿namespace EFilm.Data.Dtos.Responses
{
    public class SearchOmdbapiResponseModel
    {
        public Search[] Search { get; set; }

        public long TotalResults { get; set; }

        public string Response { get; set; }
    }
    public partial class Search
    {
        public string Title { get; set; }

        public string Year { get; set; }

        public string ImdbId { get; set; }

        public TypeEnum Type { get; set; }

        public Uri Poster { get; set; }
    }
    public enum TypeEnum { Game, Movie, Series };
}
