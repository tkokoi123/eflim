﻿using System.ComponentModel.DataAnnotations;

namespace EFilm.Data.Dtos.Responses
{
    public class ListMovieResponseModel
    {
        [Required]
        public Guid MovieId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string ThumbnailUrl { set; get; }
    }
}
