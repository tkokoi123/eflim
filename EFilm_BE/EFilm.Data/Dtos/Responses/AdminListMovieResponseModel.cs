﻿using EFilm.Infrastructure.Helpers;

namespace EFilm.Data.Dtos.Responses
{
    public class AdminListMovieResponseModel
    {
        public IEnumerable<AdminMovieResponseModel> AdminMovieResponseModel { get; set; }
        public Pagination Pagination { get; set; }

        public AdminListMovieResponseModel(IEnumerable<AdminMovieResponseModel> adminMovieResponseModel, Pagination pagination)
        {
            AdminMovieResponseModel = adminMovieResponseModel;
            Pagination = pagination;
        }
    }
}
