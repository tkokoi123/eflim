﻿namespace EFilm.Data.Dtos.Responses
{
    public class OmdbapiDetailsModel
    {
        public string Title { get; set; }
        public string Year { get; set; }
        public string Rated { get; set; }
        public string Released { get; set; }
        public string Runtime { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
        public string Writer { get; set; }
        public string Actors { get; set; }
        public string Plot { get; set; }
        public string Language { get; set; }
        public string Awards { get; set; }
        public Uri Poster { get; set; }
        public Rating[] Ratings { get; set; }
        public string Metascore { get; set; }
        public string ImdbRating { get; set; }
        public string ImdbVotes { get; set; }
        public string ImdbId { get; set; }
        public string Type { get; set; }
        public long TotalSeasons { get; set; }
        public string Response { get; set; }

        public OmdbapiDetailsModel()
        {
            
        }

        public OmdbapiDetailsModel(string title, string year, string rated, string released, string runtime, string genre, string director, string writer, string actors, string plot, string language, string awards, Uri poster, Rating[] ratings, string metascore, string imdbRating, string imdbVotes, string imdbId, string type, long totalSeasons, string response)
        {
            Title = title;
            Year = year;
            Rated = rated;
            Released = released;
            Runtime = runtime;
            Genre = genre;
            Director = director;
            Writer = writer;
            Actors = actors;
            Plot = plot;
            Language = language;
            Awards = awards;
            Poster = poster;
            Ratings = ratings;
            Metascore = metascore;
            ImdbRating = imdbRating;
            ImdbVotes = imdbVotes;
            ImdbId = imdbId;
            Type = type;
            TotalSeasons = totalSeasons;
            Response = response;
        }
    }
    public partial class Rating
    {
        public string Source { get; set; }
        public string Value { get; set; }

        public Rating()
        {
            
        }

        public Rating(string source, string value)
        {
            Source = source;
            Value = value;
        }
    }
    public class OmdbapiDetailsResponseModel
    {
        public string Title { get; set; }
        public string Overview { get; set; }
        public DateTime ReleasedDate { get; set; }
        public string Showtimes { get; set; }
        public List<string> Directors { get; set; }
        public List<string> Actors { get; set; }
        public List<string> Genres { get; set; }

        public OmdbapiDetailsResponseModel()
        {
            
        }

        public OmdbapiDetailsResponseModel(string title, string overview, DateTime releasedDate, string showtimes, List<string> directors, List<string> actors, List<string> genres)
        {
            Title = title;
            Overview = overview;
            ReleasedDate = releasedDate;
            Showtimes = showtimes;
            Directors = directors;
            Actors = actors;
            Genres = genres;
        }
    }

}
