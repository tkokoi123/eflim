﻿using System.ComponentModel.DataAnnotations;

namespace EFilm.Data.Dtos.Responses
{
    public class MovieDisplayResponseModel
    {
        [Required]
        public Guid MovieId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string ThumbnailUrl { set; get; }
        [Required]
        //kieu vietsub or thuyet minh, viet sub tap 19 ....
        public string Status { get; set; }
    }
}
