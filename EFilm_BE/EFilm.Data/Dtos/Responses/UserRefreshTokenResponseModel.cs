﻿namespace EFilm.Data.Dtos.Responses
{
    public class UserRefreshTokenResponseModel
    {
        public string RefreshToken { get; set; }

        public UserRefreshTokenResponseModel(string refreshToken)
        {
            RefreshToken = refreshToken;
        }
    }
}
