﻿namespace EFilm.Data.Dtos.Responses
{
    public class MovieEpisodeResponseModel
    {
        public Guid MovieEpisodeId { get; set; }
        public Guid MovieId { get; set; }
        public string ObjectUrl { set; get; }
        public int Episode { get; set; }
    }
}
