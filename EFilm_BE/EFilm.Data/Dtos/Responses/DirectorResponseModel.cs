﻿namespace EFilm.Data.Dtos.Responses
{
    public class DirectorResponseModel
    {
        public Guid DirectorId { get; set; }
        public string DirectorName { get; set; }

        public DirectorResponseModel()
        {
            
        }

        public DirectorResponseModel(Guid directorId, string directorName)
        {
            DirectorId = directorId;
            DirectorName = directorName;
        }
    }
}
