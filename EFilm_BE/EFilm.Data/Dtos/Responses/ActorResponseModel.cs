﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFilm.Data.Dtos.Responses
{
    public class ActorResponseModel
    {
        public Guid ActorId { get; set; }
        public string ActorName { get; set; }
    }
}
