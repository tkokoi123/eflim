﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFilm.Data.Dtos.Responses
{
    public class CountryResponseModel
    {
        public Guid CountryId { get; set; }
        public string CountryName { get; set; }
    }
}
