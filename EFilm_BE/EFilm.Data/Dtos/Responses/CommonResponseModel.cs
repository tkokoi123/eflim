﻿namespace EFilm.Data.Dtos.Responses
{
    public class CommonResponseModel
    {
        public string Message { get; set; }

        public CommonResponseModel(string message)
        {
            Message = message;
        }
    }
}
