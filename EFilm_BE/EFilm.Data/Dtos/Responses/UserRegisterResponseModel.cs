﻿namespace EFilm.Data.Dtos.Responses
{
    public class UserRegisterResponseModel
    {
        public string Message { get; set; }

        public UserRegisterResponseModel(string message)
        {
            Message = message;
        }
    }
}
