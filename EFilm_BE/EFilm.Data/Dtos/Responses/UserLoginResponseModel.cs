﻿using System.ComponentModel.DataAnnotations;

namespace EFilm.Data.Dtos.Responses
{
    public class UserLoginResponseModel
    {
        [Required]
        public string AccessToken { get; set; }
        [Required]
        public string RefreshToken { get; set; }

        public UserLoginResponseModel(string accessToken, string refreshToken)
        {
            AccessToken = accessToken;
            RefreshToken = refreshToken;
        }

        public UserLoginResponseModel()
        {
        }
    }
}
