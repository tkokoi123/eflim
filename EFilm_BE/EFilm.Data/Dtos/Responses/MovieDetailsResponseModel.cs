﻿using EFilm.Data.Dtos.Responses;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFilm.Data.Entities
{
    public class MovieDetailsResponseModel : BaseEntity
    {
        public Guid MovieId { get; set; }
        public string Title { get; set; }
        public string Overview { get; set; }
        public short ReleaseYear { get; set; }
        public string Showtimes { get; set; }
        public double VoteAvg { get; set; }
        public int VoteCount { get; set; }
        public string ThumbnailUrl { set; get; }
        public TypeMovie TypeMovie { get; set; }
        public TypeSubtitle TypeSubtitle { get; set; }
        public  CountryResponseModel Country { get; set; }
        public  IEnumerable<DirectorResponseModel> Directors { get; set; }
        public  IEnumerable<TagResponseModel> Tags { get; set; }
        public  IEnumerable<ActorResponseModel> Actors { get; set; }
        public  IEnumerable<GenreResponseModel> Genres { get; set; }
        public  IEnumerable<MovieEpisodeResponseModel> Episodes { get; set; }
        public  IEnumerable<CommentResponseModel> Comments { get; set; }
    }

}
