﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFilm.Data.Dtos.Responses
{
    public class GenreResponseModel
    {
        public Guid GenreId { get; set; }
        public string GenreName { get; set; }
    }
}
