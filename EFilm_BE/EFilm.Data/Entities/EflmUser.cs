﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFilm.Data.Entities
{
    [Table("EFLM_User")]
    public class EflmUser : BaseEntity
    {
        public Guid UserId { get; set; }

        [MaxLength(20)]
        public string Username { get; set; }

        [Required]
        [MaxLength(255)]
        public string Password { get; set; }

        [MaxLength(255)]
        public string Email { get; set; }

        [Required]
        [MaxLength(1000)]
        public string Fullname { get; set; }

        [MaxLength(20)]
        public string Mobile { get; set; }
        public short Status { get; set; }
        public short Role { get; set; }

        public virtual EflmToken Token { get; set; }
        public virtual ICollection<EflmComment> Comments { get; set; }

        public EflmUser()
        {
        }

        public EflmUser(Guid userId, string username, string password, string email, string fullname, string mobile, short status, short role, EflmToken token)
        {
            UserId = userId;
            Username = username;
            Password = password;
            Email = email;
            Fullname = fullname;
            Mobile = mobile;
            Status = status;
            Role = role;
            Token = token;
        }
    }
}
