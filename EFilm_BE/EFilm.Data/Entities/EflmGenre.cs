﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFilm.Data.Entities
{
    [Table("EFLM_Genre")]
    public class EflmGenre : BaseEntity
    {
        public Guid GenreId { get; set; }

        [MaxLength(255)]
        [Required]
        public string GenreName { get; set; }
        public virtual ICollection<EflmMovieGenre> MovieGenres { get; set; }
    }
}
