﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EFilm.Data.Entities
{
    [Table("EFLM_Token")]
    public class EflmToken
    {
        public Guid UserId { get; set; }
        public string Token { get; set; }
        public DateTime ExpriedDate { get; set; }

        public virtual EflmUser User { get; set; }

        public EflmToken()
        {
        }

        public EflmToken(Guid userId, string token, DateTime expriedDate)
        {
            UserId = userId;
            Token = token;
            ExpriedDate = expriedDate;
        }
    }
}
