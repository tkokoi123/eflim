﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFilm.Data.Entities
{
    [Table("EFLM_MovieDirector")]
    public class EflmMovieDirector : BaseEntity
    {
        public Guid MovieId { get; set; }
        public Guid DirectorId { get; set; }
        public virtual EflmDirector Director { get; set; }
        public virtual EflmMovie Movie { get; set; }
    }
}
