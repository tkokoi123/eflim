﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFilm.Data.Entities
{
    [Table("EFLM_MovieGenre")]
    public class EflmMovieGenre : BaseEntity
    {
        public Guid GenreId { get; set; }
        public Guid MovieId { get; set; }
        public virtual EflmGenre Genre { get; set; }
        public virtual EflmMovie Movie { get; set; }

    }
}
