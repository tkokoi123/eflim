﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFilm.Data.Entities
{
    [Table("EFLM_Tag")]
    public class EflmTag : BaseEntity
    {
        public Guid TagId { get; set; }
        [MaxLength(255)]
        [Required]
        public string? TagName { get; set; }
        public virtual ICollection<EflmMovieTag> MovieTags { get; set; }
    }
}
