﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFilm.Data.Entities
{
    [Table("EFLM_Director")]
    public class EflmDirector : BaseEntity
    {
        public Guid DirectorId { get; set; }
        [MaxLength(255)]
        public string DirectorName { get; set; }
        public virtual ICollection<EflmMovieDirector> MovieDirectors { get; set; }
    }
}
