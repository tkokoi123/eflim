﻿using EFilm.Data.Entities.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace EFilm.Data.Entities
{
    public class BaseEntity : IDateTracking, ISoftDateTracking
    {
        public bool IsDelete { get; set; }
        [MaxLength(20)]
        public string CreateUser { get; set; } 
        public DateTime CreateDate { get; set; }
        [MaxLength(20)]
        public string UpdateUser { get; set; }
        public DateTime UpdateDate { get; set; }

    }
}
