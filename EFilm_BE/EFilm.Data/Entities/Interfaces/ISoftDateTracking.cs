﻿namespace EFilm.Data.Entities.Interfaces
{
    public interface ISoftDateTracking
    {
        public bool IsDelete { get; set; }
    }
}
