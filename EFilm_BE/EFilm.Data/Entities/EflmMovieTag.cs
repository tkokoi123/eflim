﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFilm.Data.Entities
{
    [Table("EFLM_MovieTag")]
    public class EflmMovieTag : BaseEntity
    {
        public Guid MovieId { get; set; }
        public Guid TagId { get; set; }
        public virtual EflmTag Tag { get; set; }
        public virtual EflmMovie Movie { get; set; }
    }
}
