﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFilm.Data.Entities
{
    [Table("EFLM_Comment")]
    public class EflmComment : BaseEntity
    {
        public Guid CommentId { get; set; }
        public Guid UserId { get; set; }
        public Guid MovieId { get; set; }
        public string Content { get; set; }
        public virtual EflmUser User { get; set; }
        public virtual EflmMovie? Movie { get; set; }

    }
}
