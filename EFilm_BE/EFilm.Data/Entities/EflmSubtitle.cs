﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFilm.Data.Entities
{
    [Table("EFLM_Subtitle")]
    public class EflmSubtitle : BaseEntity
    {
        public Guid SubtitleId { get; set; }
        public Guid MovieEpisodeId { get; set; }
        [MaxLength(30)]
        public string Language { get; set; }
        [MaxLength(255)]
        public string FolderName { get; set; }
        public virtual EflmMovieEpisode MovieEpisode { get; set; }
    }
}
