﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EFilm.Data.Entities
{
    [Table("EFLM_Processing")]
    public class EflmProcessing : BaseEntity
    {
        public Guid ProcessingId { get; set; }
        public Guid MovieEpisodeId { get; set; }
        public short Status { get; set; }
        public TimeOnly StartTime { get; set; }
        public TimeOnly FinishTime { get; set; }
        public string Message { get; set; }
        public virtual EflmMovieEpisode MovieEpisode { get; set; }

        public EflmProcessing()
        {

        }

        public EflmProcessing(Guid processingId, Guid movieEpisodeId, short status, TimeOnly startTime, TimeOnly finishTime, string message)
        {
            ProcessingId = processingId;
            MovieEpisodeId = movieEpisodeId;
            Status = status;
            StartTime = startTime;
            FinishTime = finishTime;
            Message = message;
        }
    }
}
