﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFilm.Data.Entities
{
    [Table("EFLM_MovieEpisode")]
    public class EflmMovieEpisode : BaseEntity
    {
        public Guid MovieEpisodeId { get; set; }
        public Guid MovieId { get; set; }
        public string ObjectUrl { set; get; }
        //defalut 0
        [Range(0, int.MaxValue)]
        public int Episode { get; set; }
        public virtual EflmMovie? Movie { get; set; }
        public virtual EflmProcessing Processing { get; set; }
        public virtual ICollection<EflmSubtitle> Subtitles { get; set; }

        public EflmMovieEpisode()
        {
            
        }

        public EflmMovieEpisode(Guid movieEpisodeId, Guid movieId, string objectUrl, int episode)
        {
            MovieEpisodeId = movieEpisodeId;
            MovieId = movieId;
            ObjectUrl = objectUrl;
            Episode = episode;
        }
    }
}
