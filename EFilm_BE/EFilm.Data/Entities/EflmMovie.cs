﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFilm.Data.Entities
{
    [Table("EFLM_Movie")]
    public class EflmMovie : BaseEntity
    {
        public Guid MovieId { get; set; }
        [MaxLength(255)]
        [Required]
        public string Title { get; set; }
        public string Overview { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Showtimes { get; set; }
        [Range(0,10)]
        public double VoteAvg { get; set; }
        public int VoteCount { get; set; }
        public string ThumbnailUrl { set; get; }
        public bool HotMovie { get; set; }
        public TypeMovie TypeMovie { get; set; }
        public TypeSubtitle TypeSubtitle { get; set; }
        public Guid CountryId { get;set; }
        public virtual EflmCountry? Country { get; set; }
        public virtual ICollection<EflmMovieDirector> MovieDirectors { get; set; }
        public virtual ICollection<EflmMovieEpisode> MovieEpisodes { get; set; }
        public virtual ICollection<EflmMovieTag> MovieTags { get; set; }
        public virtual ICollection<EflmMovieActor> MovieActors { get; set; }
        public virtual ICollection<EflmMovieGenre> MovieGenres { get; set; }
        public virtual ICollection<EflmComment> Comments { get; set; }

        public EflmMovie()
        {
            
        }

        public EflmMovie(Guid movieId, string title, string overview, DateTime releaseDate, string showtimes, double voteAvg, int voteCount, string thumbnailUrl, TypeMovie typeMovie, TypeSubtitle typeSubtitle, Guid countryId, EflmCountry? country, ICollection<EflmMovieDirector> movieDirectors, ICollection<EflmMovieEpisode> movieEpisodes, ICollection<EflmMovieTag> movieTags, ICollection<EflmMovieActor> movieActors, ICollection<EflmMovieGenre> movieGenres, ICollection<EflmComment> comments)
        {
            MovieId = movieId;
            Title = title;
            Overview = overview;
            ReleaseDate = releaseDate;
            Showtimes = showtimes;
            VoteAvg = voteAvg;
            VoteCount = voteCount;
            ThumbnailUrl = thumbnailUrl;
            TypeMovie = typeMovie;
            TypeSubtitle = typeSubtitle;
            CountryId = countryId;
            Country = country;
            MovieDirectors = movieDirectors;
            MovieEpisodes = movieEpisodes;
            MovieTags = movieTags;
            MovieActors = movieActors;
            MovieGenres = movieGenres;
            Comments = comments;
        }
    }
    public enum TypeMovie
    {
        None=0,
        Serises=1,
        Movie=2
    }
    public enum TypeSubtitle
    {
        None=0,
        Narration=1,
        Subtitle=2
    }
}
