﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFilm.Data.Entities
{
    [Table("EFLM_Actor")]
    public class EflmActor : BaseEntity
    {
        public Guid ActorId { get; set; }
        [MaxLength(255)]
        [Required]
        public string ActorName { get; set; }
        public virtual ICollection<EflmMovieActor> MovieActors { get; set; }
    }
}
