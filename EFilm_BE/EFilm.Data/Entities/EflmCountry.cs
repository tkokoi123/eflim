﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFilm.Data.Entities
{
    [Table("EFLM_Country")]
    public class EflmCountry 
    {
        public Guid CountryId { get; set; }
        [MaxLength(255)]
        [Required]
        public string CountryName { get; set; }
        public virtual ICollection<EflmMovie>? Movies { get; set; }
    } 
}
