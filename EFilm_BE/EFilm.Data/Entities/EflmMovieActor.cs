﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFilm.Data.Entities
{
    [Table("EFLM_MovieActor")]
    public class EflmMovieActor : BaseEntity
    {
        public Guid MovieId { get; set; }
        public Guid ActorId { get; set; }
        public virtual EflmActor Actor { get; set; }
        public virtual EflmMovie Movie { get; set; }
    }
}
