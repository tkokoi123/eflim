﻿using AutoMapper;
using EFilm.Application.AutoMapper;
using EFilm.Application.Services;
using EFilm.Application.Services.BackgroundServices;
using EFilm.Application.Services.Interfaces;
using EFilm.Application.Services.Interfaces.MovieManagement;
using EFilm.Application.Services.MovieManagement;
using EFilm.Data.EF;
using EFilm.Data.EF.Repositories;
using EFilm.Infrastructure.Configurations;
using EFilm.Infrastructure.Helpers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Channels;
using static EFilm.Infrastructure.Enums.SystemEnum;

namespace EFilm.DependencyConfig
{
    public static class DependencyInjection
    {
        public static IServiceCollection CommonService(this IServiceCollection services, IConfiguration configuration)
        {
            //HttpContextAcessor
            services.AddHttpContextAccessor();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddDbContext<AppDbContext>(option => option.UseNpgsql(configuration.GetConnectionString("Default"),
                                                            o => o.MigrationsAssembly("PCF.Data.EF")), ServiceLifetime.Scoped);

            //Config CORS
            services.AddCors(cors => cors.AddPolicy("EfilmPolicy", builder =>
            {
                builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.SaveToken = true;
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidAudience = AppSettings.ValidAudience,
                        ValidIssuer = AppSettings.ValidIssuer,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:AccessTokenSecret"]))
                    };
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("AdminRole",
                    policy => policy.RequireRole(UserRoles.Admin.ToString(), UserRoles.User.ToString()));

                options.AddPolicy("UserRole",
                    policy => policy.RequireRole(UserRoles.User.ToString()));
            });

            // Add services to the container.
            services
                .AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.DictionaryKeyPolicy = System.Text.Json.JsonNamingPolicy.CamelCase;
                    options.JsonSerializerOptions.PropertyNamingPolicy = System.Text.Json.JsonNamingPolicy.CamelCase;
                    options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
                    //options.JsonSerializerOptions.MaxDepth = 5;
                }).ConfigureApiBehaviorOptions(options =>
                {
                    options.InvalidModelStateResponseFactory = context =>
                    {
                        return ApiFormatError.CustomErrorResponse(context);
                    };
                });
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Version = "v1",
                    Title = "Eflim Api",

                });
            });
            services.AddHealthChecks();

            services.Configure<KestrelServerOptions>(options =>
            {
                options.Limits.MaxRequestBodySize = int.MaxValue; // if don't set default value is: 30 MB
            });

            services.Configure<FormOptions>(x =>
            {
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = long.MaxValue; // if don't set default value is: 128 MB
                x.MultipartHeadersLengthLimit = int.MaxValue;
            });

            return services;
        }

        public static IServiceCollection AutoMapperService(this IServiceCollection services)
        {
            services.AddAutoMapper(config =>
            {
                config.AddProfile<DomainToResponseModelMappingProfile>();
                config.AddProfile<RequestModelToDomainMappingProfile>();
                config.AddProfile<ResponseModelToDomainMappingProfile>();
            });

            services.AddScoped<IMapper>(builder => new Mapper(builder.GetRequiredService<AutoMapper.IConfigurationProvider>(), builder.GetService));
            return services;
        }

        public static IServiceCollection ProfilerService(this IServiceCollection services)
        {
            //Config Profiler
            services.AddMemoryCache();
            //URL: profiler/results-index
            services.AddMiniProfiler(config => config.RouteBasePath = "/profiler")
                .AddEntityFramework();

            return services;
        }

        public static IServiceCollection SignalRService(this IServiceCollection services)
        {
            services.AddSignalR();
            return services;
        }

        public static IServiceCollection ApplicationService(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IOmdbapiService, OmdbapiService>();
            services.AddScoped<IEflmMovieService, EflmMovieService>();

            services.AddScoped<IEflmProcessingService, EflmProcessingService>();
            services.AddScoped<IEflmUserService, EflmUserService>();
            services.AddScoped<IEflmTokenService, EflmTokenService>();
            services.AddScoped<IEflmGenreService, EflmGenreService>();
            services.AddScoped<IEflmCountryService, EflmCountryService>();
            services.AddScoped<IEflmDirectorService, EflmDirectorService>();

            services.AddTransient<IS3StorageService, S3StorageService>();

            services.AddHostedService<VideoProcessing>();
            services.AddSingleton(_ => Channel.CreateUnbounded<VideoProcessingMessage>());
            return services;
        }

        public static IServiceCollection CacheService(this IServiceCollection services)
        {
            services.AddStackExchangeRedisCache(o =>
            {
                o.Configuration = AppSettings.Redis;
            });
            services.AddSingleton<ICacheService, CacheService>();
            return services;
        }

        public static IServiceCollection HttpClientService(this IServiceCollection services)
        {
            services.AddHttpClient(AppSettings.OmdbName, client =>
            {
                client.BaseAddress = new Uri(AppSettings.OmdbUrl);
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
            });

            return services;
        }
    }
}
