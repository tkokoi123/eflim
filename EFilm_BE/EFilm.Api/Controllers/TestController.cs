﻿using EFilm.Application.Services.BackgroundServices;
using EFilm.Application.Services.Interfaces;
using EFilm.Data.Dtos.Requests;
using EFilm.Data.EF;
using EFilm.Data.S3Obj;
using EFilm.Infrastructure.Configurations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using System.Threading.Channels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EFilm.Controllers
{
    [ApiController]
    public class TestController : BaseApiController
    {
        private readonly ILogger<TestController> _logger;
        private readonly IDistributedCache _distributedCache;
        private readonly IWebHostEnvironment _env;
        private readonly IS3StorageService _storageService;
        private readonly IUnitOfWork _unitOfWork;

        public TestController(ILogger<TestController> logger,
            IDistributedCache distributedCache, IWebHostEnvironment env, IS3StorageService storageService, IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _distributedCache = distributedCache;
            _env = env;
            _storageService = storageService;
            _unitOfWork = unitOfWork;
        }

        [HttpGet("~/api/download")]
        public IActionResult DownLoad()
        {
            var filePath = $"E:\\Installer\\Age.of.Empires.III.Complete.Collection.(downmienphi.com).zip";
            var fileStream = new FileStream(filePath, FileMode.Open);
            return File(fileStream, "video/x-matroska", "download");
        }

        [HttpGet("~/api/addcache")]
        public async Task<IActionResult> AddCache(string key, string value)
        {
            await _distributedCache.SetStringAsync(key, value).ConfigureAwait(false);
            return Ok();
        }

        [HttpGet("~/api/getcache")]
        public async Task<IActionResult> GetCache(string key)
        {
            var result = await _distributedCache.GetStringAsync(key).ConfigureAwait(false);
            return Ok(result);
        }

        ////[HttpGet("~/api/playback/{subFolder}/{fileName}")]
        ////public IActionResult PlayBackVideo([FromRoute] string subFolder, [FromRoute] string fileName)
        ////{
        ////    string pathfile = Path.Combine(_env.WebRootPath, "Output", subFolder, fileName);

        ////    Console.WriteLine(pathfile);
        ////    var fileStream = new FileStream(pathfile, FileMode.Open, FileAccess.Read);
        ////    return File(fileStream, "application/x-mpegURL", "output.m3u8");
        ////}

        ////[HttpGet("~/api/playback/{subFolder}")]
        ////public IActionResult PlayBackVideo([FromRoute] string subFolder)
        ////{

        ////    string pathfile = Path.Combine(_env.WebRootPath, "Output", subFolder);

        ////    var fileStream = new FileStream(pathfile, FileMode.Open, FileAccess.Read);
        ////    return File(fileStream, "application/x-mpegURL", "output.m3u8");
        ////}

        //[HttpGet("~/api/playback-s3/{filmId}/{subFolder}/{fileName}")]
        //[Produces("application/x-mpegURL")]
        //public async Task<IActionResult> PlayBackVideoS3([FromRoute] string subFolder, [FromRoute] string fileName, [FromRoute] string filmId)
        //{
        //    var fileStream = await _storageService.DownloadFileContent(new S3RquestDirectory
        //    {
        //        BucketName = AppSettings.ObjectStorageBucket,
        //        Prefix = $"efilm/{filmId}/{subFolder}/{fileName}"
        //    });

        //    return File(fileStream, "application/x-mpegURL");
        //}

        //[HttpGet("~/api/playback-s3/{filmId}/{fileName}")]
        //[Produces("application/x-mpegURL")]
        //public async Task<IActionResult> PlayBackVideoS3([FromRoute] string filmId, [FromRoute] string fileName)
        //{
        //    var fileStream = await _storageService.DownloadFileContent(new S3RquestDirectory
        //    {
        //        BucketName = AppSettings.ObjectStorageBucket,
        //        Prefix = $"efilm/{filmId}/{fileName}"
        //    });

        //    return File(fileStream, "application/x-mpegURL");
        //}

        [HttpPost("~/api/s3/upload")]
        public async Task<IActionResult> UploadVideo([FromForm] IFormFile file)
        {

            // Process file
            await using var memoryStream = new MemoryStream();
            await file.CopyToAsync(memoryStream);

            var fileExt = Path.GetExtension(file.FileName);
            var docName = $"{Guid.NewGuid().ToString()}{fileExt}";

            var request = new S3RequestData()
            {
                BucketName = AppSettings.ObjectStorageBucket,
                InputStream = memoryStream,
                Name = docName,
            };
            bool isSuccess = await _storageService.UploadFileAsync(request).ConfigureAwait(false);
            if (!isSuccess)
            {
                return BadRequest("Can't upload file!");
            }
            return Ok(docName);
        }

        [HttpPost("~/api/s3/geturl")]
        public IActionResult GetUrl([FromBody] GetUrlRequestModel filename)
        {
            var request = new S3RequestData()
            {
                BucketName = AppSettings.ObjectStorageBucket,
                InputStream = null,
                Name = filename.Filename,
            };
            string url = _storageService.GetFileUrl(request);
            return Ok(url);
        }

        [HttpDelete("~/api/s3/delete")]
        public async Task<IActionResult> DeleteObject([FromBody] GetUrlRequestModel fileName)
        {
            var request = new S3RquestDirectory()
            {
                BucketName = AppSettings.ObjectStorageBucket,
                Prefix = $"efilm/{fileName.Filename}"
            };
            bool isDeleted = await _storageService.DeleteDirectory(request);
            if (isDeleted)
            {
                return Ok(fileName);
            }
            return NotFound(fileName);
        }

        [HttpGet("~/api/count-task")]
        public IActionResult CountTask([FromServices] Channel<VideoProcessingMessage> channel)
        {
            return Ok(channel.Reader.Count);
        }

        [HttpGet("~/api/test-admin")]
        [Authorize(Roles = "Admin")]
        public IActionResult TestAdmin()
        {
            return Ok("You are admin");
        }

        [HttpGet("~/api/test-user")]
        [Authorize(Roles = "Admin,User")]
        public IActionResult TestUser()
        {
            return Ok("You are user");
        }

    }
}
