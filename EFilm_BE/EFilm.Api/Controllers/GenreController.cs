﻿using EFilm.Application.Services.Interfaces;
using EFilm.Application.Services.Interfaces.MovieManagement;
using EFilm.Application.Services.MovieManagement;
using EFilm.Controllers;
using EFilm.Data.Dtos.Requests;
using Microsoft.AspNetCore.Mvc;

namespace EFilm.Api.Controllers
{
    [ApiController]
    public class GenreController : BaseApiController
    {
        private readonly IEflmGenreService _IEflmGenreService;
        public GenreController(IEflmGenreService IEflmGenreService)
        {
            _IEflmGenreService = IEflmGenreService;
        }

        [HttpGet("~/api/v1/genre-management/all")]
        public async Task<IActionResult> GetGenres()
        {
            var result = await _IEflmGenreService.GetGenres()
                .ConfigureAwait(false);
            if (result == null)
            {
                return NotFound("Genres is empty!");
            }
            return Ok(result);
        }

    }
}
