﻿using EFilm.Application.Services.Interfaces;
using EFilm.Application.Services.Interfaces.MovieManagement;
using EFilm.Application.Services.MovieManagement;
using EFilm.Controllers;
using EFilm.Data.Dtos.Requests;
using Microsoft.AspNetCore.Mvc;

namespace EFilm.Api.Controllers
{
    [ApiController]
    public class CountryController : BaseApiController
    {
        private readonly IEflmCountryService _IEflmCountryService;
        public CountryController(IEflmCountryService IEflmCountryService)
        {
            _IEflmCountryService = IEflmCountryService;
        }

        [HttpGet("~/api/v1/country-management/all")]
        public async Task<IActionResult> GetCountrys()
        {
            var result = await _IEflmCountryService.GetCountrys()
                .ConfigureAwait(false);
            
            if (result == null) {
                return NotFound("Country is empty!!");
            }
            return Ok(result);
        }

    }
}
