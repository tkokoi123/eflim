﻿using EFilm.Application.Services.Interfaces;
using EFilm.Controllers;
using EFilm.Data.Dtos;
using EFilm.Data.Dtos.Requests;
using EFilm.Data.Dtos.Responses;
using EFilm.Infrastructure.Configurations;
using EFilm.Infrastructure.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.IdentityModel.JsonWebTokens;
using System.Collections.ObjectModel;
using System.Security.Claims;
using static EFilm.Infrastructure.Enums.SystemEnum;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EFilm.Api.Controllers
{
    [ApiController]
    public class UserController : BaseApiController
    {
        private readonly IEflmUserService _userService;
        private readonly ICacheService _cacheService;
        private readonly ITokenService _tokenService;
        private readonly IEflmTokenService _eflmTokenService;

        public UserController(IEflmUserService userService, ICacheService cacheService, ITokenService tokenService, IEflmTokenService eflmTokenService)
        {
            _userService = userService;
            _cacheService = cacheService;
            _tokenService = tokenService;
            _eflmTokenService = eflmTokenService;
        }

        [AllowAnonymous]
        [HttpPost(CommonRouter.CommonLogin)]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Login([FromBody] UserLoginRequestModel userRq)
        {
            //User login
            var user = await _userService.UserLogin(userRq);

            if (user is null)
            {
                return BadRequest("Wrong user name or password!");
            }

            //Create claims
            var userRole = (UserRoles)user.Role;
            var authClaims = new Collection<Claim>
            {
                    new Claim(JwtRegisteredClaimNames.Name, userRq.Username),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.Role, userRole.ToString())
            };

            var accessToken = _tokenService.GenerateAccessToken(authClaims);
            var refreshToken = _tokenService.GenerateRefreshToken();

            var expiredTime = DateTime.UtcNow.AddDays(AppSettings.RefreshTokenExpriedTime);

            DistributedCacheEntryOptions options = new()
            {
                AbsoluteExpiration = DateTime.UtcNow.AddDays(AppSettings.RefreshTokenExpriedTime)
            };

            bool isSuccess = await _eflmTokenService.SaveRefeshTokenDb(user.UserId, refreshToken, expiredTime)
                .ConfigureAwait(false);

            if (isSuccess)
            {
                var userCache = new CacheServiceDto(refreshToken, expiredTime);
                //Add refresh token to cache
                await _cacheService.SetAsync($"member_rf_{user.UserId}", userCache, options)
                .ConfigureAwait(false);
            }

            return Ok(new UserLoginResponseModel(accessToken, refreshToken));


        }

        [AllowAnonymous]
        [HttpPost(CommonRouter.CommonRegister)]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Register([FromBody] UserRegisterRequestModel userRq)
        {
            var isValid = await _userService.UserRegister(userRq)
                .ConfigureAwait(false);

            if (isValid)
            {
                return Ok("Register successfully!");
            }
            return BadRequest("User already exist!");

        }

        [Authorize]
        [HttpPost(CommonRouter.CommonLogout)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Logout()
        {
            //Todo: Delete cache, delete database

            return NoContent();
        }

        [HttpPost(CommonRouter.CommonRefreshToken)]
        [Consumes("application/json")]
        public async Task<IActionResult> RefreshToken([FromBody] UserRefreshTokenRequestModel userRq)
        {
            var principal = _tokenService.GetPrincipalFromExpriedToken(userRq.AccessToken);

            if (principal is null)
            {
                return BadRequest("Invalid access token or refresh token");
            }

            string? username = principal.Identity?.Name;

            var user = await _userService.FindUserByUsername(username)
                .ConfigureAwait(false);

            if (user is null)
            {
                return BadRequest("Invalid access token or refresh token");
            }

            //Get refresh token in cache first
            var refreshTokenCacheInfo = await _cacheService.GetAsync<CacheServiceDto>($"member_rf_{user.UserId}")
                .ConfigureAwait(false);

            if (refreshTokenCacheInfo is null)
            {
                //Get refresh token in db
                var userRefreshToken = await _eflmTokenService.GetTokenInfo(user.UserId)
                    .ConfigureAwait(false);

                if (userRefreshToken is not null)
                {
                    refreshTokenCacheInfo = new CacheServiceDto(userRefreshToken.Token, userRefreshToken.ExpriedDate);
                }
            }

            if (refreshTokenCacheInfo is null || !refreshTokenCacheInfo.Token.Equals(userRq.RefreshToken) || refreshTokenCacheInfo.ExpriedTime <= DateTime.UtcNow)
            {
                return BadRequest("Invalid access token or refresh token");
            }

            var userRole = (UserRoles)user.Role;
            var authClaims = new Collection<Claim>
            {
                    new Claim(JwtRegisteredClaimNames.Name, user.Username),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.Role, userRole.ToString())
            };

            var newAccessToken = _tokenService.GenerateAccessToken(authClaims);

            return Ok(new UserLoginResponseModel(newAccessToken, userRq.RefreshToken));
        }


    }
}
