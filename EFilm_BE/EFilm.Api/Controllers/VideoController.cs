﻿using EFilm.Application.Services.BackgroundServices;
using EFilm.Application.Services.Interfaces;
using EFilm.Application.Services.Interfaces.MovieManagement;
using EFilm.Controllers;
using EFilm.Data.Entities;
using EFilm.Data.S3Obj;
using EFilm.Infrastructure.Configurations;
using EFilm.Infrastructure.Constants;
using EFilm.Infrastructure.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Channels;
using static EFilm.Infrastructure.Enums.SystemEnum;

namespace EFilm.Api.Controllers
{
    [ApiController]
    public class VideoController : BaseApiController
    {
        private readonly IWebHostEnvironment _env;
        private readonly IS3StorageService _storageService;
        private readonly IEflmProcessingService _eflmProcessingService;
        private readonly IEflmMovieService _eflmMovieService;

        public VideoController(IWebHostEnvironment env, IS3StorageService storageService,
            IEflmProcessingService eflmProcessingService, IEflmMovieService eflmMovieService)
        {
            _env = env;
            _storageService = storageService;
            _eflmProcessingService = eflmProcessingService;
            _eflmMovieService = eflmMovieService;
        }

        [HttpPost(AdminRouter.UploadVideo)]
        [Authorize(Roles = "Admin")]
        [DisableRequestSizeLimit]
        [Consumes("multipart/form-data")]
        public async Task<IActionResult> UploadVideo([FromForm] IFormFile file, 
            [FromForm] Guid filmId, [FromForm] int episodeNum,
            [FromServices] Channel<VideoProcessingMessage> channel)
        {
            var mime = file.FileName.Split('.').Last();

            if (!FileHelper.CheckFileSignature(file, mime))
            {
                return BadRequest("File not valid!");
            }

            var guidFile = Guid.NewGuid();
            var fileName = string.Concat($"temp_{guidFile.ToString()}", ".", mime);

            var savePath = Path.Combine(_env.WebRootPath, "Temp", fileName);

            Guid episodeId = await _eflmMovieService.AddFilmEpisode(filmId, episodeNum)
                .ConfigureAwait(false);

            await using (var fileStream = new FileStream(savePath, FileMode.Create, FileAccess.Write))
            { 
                await file.CopyToAsync(fileStream);
                var process = new EflmProcessing(Guid.Empty, episodeId, (short)ProcessStatus.Pending, 
                    TimeOnly.FromDateTime(DateTime.UtcNow), TimeOnly.FromDateTime(DateTime.UtcNow), string.Empty);
                var data = await _eflmProcessingService.AddProcessing(process)
                    .ConfigureAwait(false);

                if (data is null)
                {
                    return BadRequest("Can't add to db");
                }

                await channel.Writer.WriteAsync(new VideoProcessingMessage
                {
                    FileId = guidFile.ToString(),
                    FilmId = episodeId.ToString(),
                    ProcessId = data.ProcessingId.ToString(),
                    OutputPath = Path.Combine(_env.WebRootPath, "Output", guidFile.ToString()),
                    InputPath = savePath,
                });
            }

            return Ok(fileName);
        }

        [HttpGet(PublicRouter.PlayBackVideo_1)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [AllowAnonymous]
        public async Task<IActionResult> PlayBackVideo([FromRoute] string subFolder, [FromRoute] string fileName, [FromRoute] string filmId)
        {
            var fileStream = await _storageService.DownloadFileContent(new S3RquestDirectory
            {
                BucketName = AppSettings.ObjectStorageBucket,
                Prefix = $"efilm/processed_{filmId}/{subFolder}/{fileName}"
            });

            if (fileStream == Stream.Null)
            {
                return NotFound("Segment not found!");
            }

            return File(fileStream, "application/x-mpegURL");
        }

        [HttpGet(PublicRouter.PlayBackVideo_2)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [AllowAnonymous]
        public async Task<IActionResult> PlayBackVideo([FromRoute] string filmId, [FromRoute] string fileName)
        {
            var fileStream = await _storageService.DownloadFileContent(new S3RquestDirectory
            {
                BucketName = AppSettings.ObjectStorageBucket,
                Prefix = $"efilm/processed_{filmId}/{fileName}"
            });

            if (fileStream == Stream.Null)
            {
                return NotFound("Segment not found!");
            }

            return File(fileStream, "application/x-mpegURL");
        }

        [HttpGet(PublicRouter.LoadFilmSubtitle)]
        [AllowAnonymous]
        public async Task<IActionResult> GetFilmSubtitle([FromRoute] string filmId, [FromRoute] string subId)
        {
            var fileStream = await _storageService.DownloadFileContent(new S3RquestDirectory
            {
                BucketName = AppSettings.ObjectStorageBucket,
                Prefix = $"efilm_subtitle/{filmId}/{subId}"
            });

            if (fileStream == Stream.Null)
            {
                return NotFound("Subtitle does not exist!");
            }

            return File(fileStream, "application/x-subrip");
        }
    }
}
