﻿using EFilm.Application.Services.Interfaces;
using EFilm.Application.Services.Interfaces.MovieManagement;
using EFilm.Application.Services.MovieManagement;
using EFilm.Controllers;
using EFilm.Data.Dtos.Requests;
using EFilm.Data.Entities;
using Microsoft.AspNetCore.Mvc;

namespace EFilm.Api.Controllers
{
    [ApiController]
    public class MovieController : BaseApiController
    {
        private readonly IOmdbapiService _IOmdbapiService;
        private readonly IEflmMovieService _IEflmMovieService;
        private readonly IEflmDirectorService _eflmDirectorService;

        public MovieController(IOmdbapiService IOmdbapiService, IEflmMovieService IEflmMovieService, IEflmDirectorService eflmDirectorService)
        {
            _IOmdbapiService = IOmdbapiService;
            _IEflmMovieService = IEflmMovieService;
            _eflmDirectorService = eflmDirectorService;
        }

        [HttpGet("~/api/v1/movie-management/search/{title}")]
        public async Task<IActionResult> SearchOmdbapi([FromRoute] string title)
        {
            var result = await _IOmdbapiService.SearchListMovieOmdbapi(title)
                .ConfigureAwait(false);
            if (result == null)
            {
                return NotFound("Omdbapi is empty!!");
            }
            return Ok(result);
        }

        [HttpGet("~/api/v1/movie-management/get/{id}")]
        public async Task<IActionResult> GetOmdbapi([FromRoute] string id)
        {
            var result = await _IOmdbapiService.GetMovieDetailOmdbapi(id)
                .ConfigureAwait(false);
            if (result == null)
            {
                return NotFound("Omdbapi is empty!!");
            }
            return Ok(result);
        }

        [HttpGet("~/api/v1/movie-filter/search/{tag}")]
        public async Task<IActionResult> SearchMovieByTagName([FromRoute] string tag)
        {
            var result = await _IEflmMovieService.GetListMovieByTagName(tag)
                .ConfigureAwait(false);
            if (result == null)
            {
                return NotFound("Movies is empty!!");
            }
            return Ok(result);
        }

        [HttpPost("~/api/v1/movie-filter")]
        public async Task<IActionResult> FilterMovie([FromBody] FilterMovieRequestModel filterRequest)
        {
            var result = await _IEflmMovieService.FilterMovie(filterRequest)
                .ConfigureAwait(false);
            if (result == null)
            {
                return NotFound("Movies is empty!!");
            }
            return Ok(result);
        }

        [HttpGet("~/api/v1/movie-filter/get-details/{movieId}")]
        public async Task<IActionResult> GetDetailsMovie([FromRoute] Guid movieId)
        {
            var result = await _IEflmMovieService.GetDetailsMovie(movieId)
                .ConfigureAwait(false);
            if (result == null)
            {
                return NotFound("Movie details is empty!!");
            }
            return Ok(result);
        }

        [HttpGet("~/api/v1/movie-filter/get-hot-movies")]
        public async Task<IActionResult> GetHotsMovie()
        {
            var result = await _IEflmMovieService.GetHotMovies()
                .ConfigureAwait(false);
            if (result == null)
            {
                return NotFound("Hot movies is empty!!");
            }
            return Ok(result);
        }

        [HttpGet("~/api/v1/movie-filter/get-new-movies-by-type-movie")]
        public async Task<IActionResult> GetMovieByTypeMovie([FromQuery] TypeMovie typeMovie)
        {
            var result = await _IEflmMovieService.GetNewMovieByTypeMovie(typeMovie)
                .ConfigureAwait(false);
            if (result == null)
            {
                return NotFound("Movies is empty!!");
            }
            return Ok(result);
        }

        [HttpPost("~/api/v1/movie/add")]
        public async Task<IActionResult> AddNewMovie([FromBody] NewMovieRequestModel userRq)
        {


            return Ok();
        }

        [HttpGet("~/api/v1/movie/get-all-paging")]
        public async Task<IActionResult> GetMovieInfo([FromQuery] int currentPage)
        {
            var data = await _IEflmMovieService.GetMovieList(currentPage);

            if (data == null)
            {
                return NotFound("No data");
            }

            return Ok(data);
        }

        [HttpGet("~/api/v1/director/all")]
        public async Task<IActionResult> GetAllDirector()
        {
            var data = await _eflmDirectorService.GetAll();

            if (!data.Any())
            {
                return NotFound("No data");
            }

            return Ok(data);
        }

        [HttpGet("~/api/v1/actor/all")]
        public async Task<IActionResult> GetAllActor()
        {
            var data = await _eflmDirectorService.GetAllActor();

            if (!data.Any())
            {
                return NotFound("No data");
            }

            return Ok(data);
        }

        [HttpGet("~/api/v1/episode/get/{movieId}")]
        public async Task<IActionResult> GetEpisodeByMovieId(Guid movieId)
        {
            var data = await _IEflmMovieService.GetMovieResponseById(movieId);

            if (!data.Any())
            {
                return NotFound("No data");
            }

            return Ok(data);
        }
    }
}
