﻿using EFilm.Data.EF.Repositories;
using EFilm.Data.EF.Repositories.Interfaces;

namespace EFilm.Data.EF
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private bool disposed = false;

        private readonly AppDbContext _context;

        private IEflmUserRepository _eflmUserRepository;
        private IEflmTokenRepository _eflmTokenRepository;
        private IEflmTagRepository _eflmTagRepository;
        private IEflmMovieTagRepository _eflmMovieTagRepository;
        private IEflmMovieRepository _eflmMovieRepository;
        private IEflmCountryRepository _eflmCountryRepository;
        private IEflmDirectorRepository _eflmDirectorRepository;
        private IEflmActorRepository _eflmActorRepository;
        private IEflmGenreRepository _eflmGenreRepository;
        private IEflmMovieEpisodeRepository _eflmMovieEpisodeRepository;
        private IEflmCommentRepository _eflmCommentRepository;
        private IEflmProcessingRepository _eflmProcessingRepository;

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
        }
        public IEflmMovieEpisodeRepository EflmMovieEpisodeRepository
        {
            get
            {
                if (this._eflmMovieEpisodeRepository is null)
                {
                    this._eflmMovieEpisodeRepository = new EflmMovieEpisodeRepository(_context);
                }
                return _eflmMovieEpisodeRepository;
            }
        }
        public IEflmCommentRepository EflmCommentRepository
        {
            get
            {
                if (this._eflmCommentRepository is null)
                {
                    this._eflmCommentRepository = new EflmCommentRepository(_context);
                }
                return _eflmCommentRepository;
            }
        }
        public IEflmGenreRepository EflmGenreRepository
        {
            get
            {
                if (this._eflmGenreRepository is null)
                {
                    this._eflmGenreRepository = new EflmGenreRepository(_context);
                }
                return _eflmGenreRepository;
            }
        }
        public IEflmActorRepository EflmActorRepository
        {
            get
            {
                if (this._eflmActorRepository is null)
                {
                    this._eflmActorRepository = new EflmActorRepository(_context);
                }
                return _eflmActorRepository;
            }
        }
        public IEflmDirectorRepository EflmDirectorRepository
        {
            get
            {
                if (this._eflmDirectorRepository is null)
                {
                    this._eflmDirectorRepository = new EflmDirectorRepository(_context);
                }
                return _eflmDirectorRepository;
            }
        }
        public IEflmCountryRepository EflmCountryRepository
        {
            get
            {
                if (this._eflmCountryRepository is null)
                {
                    this._eflmCountryRepository = new EflmCountryRepository(_context);
                }
                return _eflmCountryRepository;
            }
        }
        public IEflmMovieRepository EflmMovieRepository
        {
            get
            {
                if (this._eflmMovieRepository is null)
                {
                    this._eflmMovieRepository = new EflmMovieRepository(_context);
                }
                return _eflmMovieRepository;
            }
        }
        public IEflmMovieTagRepository EflmMovieTagRepository
        {
            get
            {
                if (this._eflmMovieTagRepository is null)
                {
                    this._eflmMovieTagRepository = new EflmMovieTagRepository(_context);
                }
                return _eflmMovieTagRepository;
            }
        }
        public IEflmTagRepository EflmTagRepository
        {
            get
            {
                if(this._eflmTagRepository is null)
                {
                    this._eflmTagRepository = new EflmTagRepository(_context);
                }
                return _eflmTagRepository;
            }
        }
        public IEflmUserRepository EflmUserRepository
        {
            
            get { 
                if (this._eflmUserRepository is null) {
                    this._eflmUserRepository = new EflmUserRepository(_context);
                } 
                return _eflmUserRepository; 
            }
        }
        public IEflmTokenRepository EflmTokenRepository
        {
            get
            {
                if (this._eflmTokenRepository is null)
                {
                    this._eflmTokenRepository = new EflmTokenRepository(_context);
                }
                return _eflmTokenRepository;
            }
        }
        public IEflmProcessingRepository EflmProcessingRepository 
        { 
            get 
            {
                if (this._eflmProcessingRepository is null)
                {
                    this._eflmProcessingRepository = new EflmProcessingRepository(_context);
                }    
                return _eflmProcessingRepository;
            } 
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task CommitAsync()
        {
            await _context.SaveChangesAsync().ConfigureAwait(false);
        }

        public void Commit()
        {
            _context.SaveChanges();
        }
    }
}
