﻿using EFilm.Data.EF.Repositories.Interfaces;

namespace EFilm.Data.EF
{
    public interface IUnitOfWork
    {
        public IEflmUserRepository EflmUserRepository { get; }
        public IEflmTokenRepository EflmTokenRepository { get; }
        public IEflmTagRepository EflmTagRepository { get; }
        public IEflmMovieTagRepository EflmMovieTagRepository { get; }
        public IEflmMovieRepository EflmMovieRepository { get; }
        public IEflmCountryRepository EflmCountryRepository { get; }
        public IEflmDirectorRepository EflmDirectorRepository { get; }
        public IEflmActorRepository EflmActorRepository { get; }
        public IEflmGenreRepository EflmGenreRepository { get; }
        public IEflmMovieEpisodeRepository EflmMovieEpisodeRepository { get; }
        public IEflmCommentRepository EflmCommentRepository { get; }
        public IEflmProcessingRepository EflmProcessingRepository { get; }

        public Task CommitAsync();

        public void Commit();
    }
}
