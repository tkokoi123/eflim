﻿using EFilm.Data.Entities;
using EFilm.Data.Entities.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using static EFilm.Infrastructure.Enums.SystemEnum;

namespace EFilm.Data.EF
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        #region DbSet
        public virtual DbSet<EflmUser> EflmUsers { get; set; }
        public virtual DbSet<EflmToken> EflmTokens { get; set; }
        public virtual DbSet<EflmMovie> EflmMovie { get; set; }
        public virtual DbSet<EflmActor> EflmActor { get; set; }
        public virtual DbSet<EflmMovieActor> EflmMovieActor { get; set; }
        public virtual DbSet<EflmTag> EflmTag { get; set; }
        public virtual DbSet<EflmMovieTag> EflmMovieTag { get; set; }
        public virtual DbSet<EflmCountry> EflmCountry { get; set; }
        public virtual DbSet<EflmDirector> EflmDirector { get; set; }
        public virtual DbSet<EflmGenre> EflmGenre { get; set; }
        public virtual DbSet<EflmMovieGenre> EflmMovieGenre { get; set; }
        public virtual DbSet<EflmMovieEpisode> EflmMovieEpisode { get; set; }
        public virtual DbSet<EflmComment> EflmComment { get; set; }
        public virtual DbSet<EflmProcessing> EflmProcessing { get; set; }
        public virtual DbSet<EflmSubtitle> EflmSubtitle { get; set; }

        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region EflmUser
            modelBuilder.Entity<EflmUser>(cfg =>
            {
                cfg.HasKey(u => new { u.Username, u.Email });

                cfg.HasIndex(u => new { u.Username, u.Email, u.UserId })
                   .IsUnique();

                cfg.Property(u => u.UserId)
                   .HasValueGenerator<GuidValueGenerator>();

                cfg.Property(u => u.IsDelete)
                   .HasDefaultValue(false);

                cfg.Property(u => u.Role)
                    .HasDefaultValue((short)UserRoles.User);

                cfg.Property(u => u.Status)
                    .HasDefaultValue((short)UserStatus.Activated);

                cfg.HasMany(u => u.Comments)
                   .WithOne(c => c.User)
                   .HasForeignKey(c => c.UserId)
                   .HasPrincipalKey(u => u.UserId);
                //cfg.HasOne(f => f.Token)
                //   .WithOne(f => f.User)
                //   .HasForeignKey<EflmToken>(f => f.UserId);

            });
            #endregion

            #region EflmToken
            modelBuilder.Entity<EflmToken>(cfg =>
            {
                cfg.HasKey(f => f.Token);

                cfg.HasIndex(f => new { f.UserId, f.Token })
                   .IsUnique();

                cfg.HasOne<EflmUser>(f => f.User)
                   .WithOne(f => f.Token)
                   .HasForeignKey<EflmToken>(f => f.UserId)
                   .HasPrincipalKey<EflmUser>(f => f.UserId);
            });
            #endregion

            #region EflmMovie
            modelBuilder.Entity<EflmMovie>(cfg =>
            {
                cfg.HasKey(m => m.MovieId);

                cfg.Property(m => m.MovieId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.HasMany(m => m.MovieDirectors)
                   .WithOne(mt => mt.Movie)
                   .HasForeignKey(mt => mt.MovieId)
                   .HasPrincipalKey(m => m.MovieId);

                cfg.HasMany(m => m.MovieTags)
                   .WithOne(mt => mt.Movie)
                   .HasForeignKey(mt => mt.MovieId)
                   .HasPrincipalKey(m => m.MovieId);

                cfg.HasMany(m => m.MovieActors)
                   .WithOne(ma => ma.Movie)
                   .HasForeignKey(ma => ma.MovieId)
                   .HasPrincipalKey(m => m.MovieId);

                cfg.HasMany(m => m.MovieGenres)
                   .WithOne(mg => mg.Movie)
                   .HasForeignKey(mg => mg.MovieId)
                   .HasPrincipalKey(m => m.MovieId);

                cfg.HasMany(m => m.MovieEpisodes)
                   .WithOne(me => me.Movie)
                   .HasForeignKey(me => me.MovieId)
                   .HasPrincipalKey(m => m.MovieId);

                cfg.HasMany(m => m.Comments)
                   .WithOne(c => c.Movie)
                   .HasForeignKey(c => c.MovieId)
                   .HasPrincipalKey(m => m.MovieId);

                cfg.Property(mg => mg.IsDelete)
                   .HasDefaultValue(false);
            });
            #endregion

            #region EflmActor
            modelBuilder.Entity<EflmActor>(cfg =>
            {
                cfg.HasKey(a => a.ActorId);

                cfg.Property(a => a.ActorId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.HasMany(a => a.MovieActors)
                    .WithOne(ma => ma.Actor)
                    .HasForeignKey(ma => ma.ActorId)
                    .HasPrincipalKey(a => a.ActorId);

                cfg.Property(a => a.IsDelete)
                   .HasDefaultValue(false);
            });
            #endregion

            #region EflmMovieActor
            modelBuilder.Entity<EflmMovieActor>(cfg =>
            {
                cfg.HasKey(ma => new { ma.MovieId, ma.ActorId });

                cfg.Property(ma => ma.IsDelete)
                   .HasDefaultValue(false);
            });
            #endregion

            #region EflmTag
            modelBuilder.Entity<EflmTag>(cfg =>
            {
                cfg.HasKey(t => t.TagId);

                cfg.Property(t => t.TagId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.HasMany(t => t.MovieTags)
                    .WithOne(mt => mt.Tag)
                    .HasForeignKey(mt => mt.TagId)
                    .HasPrincipalKey(t => t.TagId);

                cfg.Property(t => t.IsDelete)
                   .HasDefaultValue(false);
            });
            #endregion

            #region EflmMovieTag
            modelBuilder.Entity<EflmMovieTag>(cfg =>
            {
                cfg.HasKey(mt => new { mt.MovieId, mt.TagId });

                cfg.Property(mt => mt.IsDelete)
                   .HasDefaultValue(false);
            });
            #endregion

            #region EflmCountry
            modelBuilder.Entity<EflmCountry>(cfg =>
            {
                cfg.HasKey(c => c.CountryId);

                cfg.Property(c => c.CountryId)
                   .HasValueGenerator<GuidValueGenerator>();

                cfg.HasMany(c => c.Movies)
                    .WithOne(m => m.Country)
                    .HasForeignKey(m => m.CountryId)
                    .HasPrincipalKey(c => c.CountryId);
            });
            #endregion

            #region EflmMovieDirector
            modelBuilder.Entity<EflmMovieDirector>(cfg =>
            {
                cfg.HasKey(ma => new { ma.MovieId, ma.DirectorId });

                cfg.Property(ma => ma.IsDelete)
                   .HasDefaultValue(false);
            });
            #endregion

            #region EflmDirector
            modelBuilder.Entity<EflmDirector>(cfg =>
            {
                cfg.HasKey(d => d.DirectorId);

                cfg.Property(d => d.DirectorId)
                   .HasValueGenerator<GuidValueGenerator>();

                cfg.HasMany(d => d.MovieDirectors)
                    .WithOne(md => md.Director)
                    .HasForeignKey(md => md.DirectorId)
                    .HasPrincipalKey(d => d.DirectorId);

                cfg.Property(d => d.IsDelete)
                   .HasDefaultValue(false);
            });
            #endregion

            #region EflmGenre
            modelBuilder.Entity<EflmGenre>(cfg =>
            {
                cfg.HasKey(g => g.GenreId);

                cfg.Property(g => g.GenreId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.HasMany(g => g.MovieGenres)
                    .WithOne(mg => mg.Genre)
                    .HasForeignKey(mg => mg.GenreId)
                    .HasPrincipalKey(g => g.GenreId);

                cfg.Property(g => g.IsDelete)
                   .HasDefaultValue(false);
            });
            #endregion

            #region EflmMovieGenre
            modelBuilder.Entity<EflmMovieGenre>(cfg =>
            {
                cfg.HasKey(mg => new { mg.GenreId, mg.MovieId });

                cfg.Property(mg => mg.IsDelete)
                   .HasDefaultValue(false);
            });
            #endregion

            #region EflmMovieEpisode
            modelBuilder.Entity<EflmMovieEpisode>(cfg =>
            {
                cfg.HasKey(me => me.MovieEpisodeId);

                cfg.Property(me => me.MovieEpisodeId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.HasOne(me => me.Processing)
                   .WithOne(p => p.MovieEpisode)
                   .HasForeignKey<EflmProcessing>(p => p.MovieEpisodeId)
                   .HasPrincipalKey<EflmMovieEpisode>(me => me.MovieEpisodeId);

                cfg.HasMany(me => me.Subtitles)
                   .WithOne(s => s.MovieEpisode)
                   .HasForeignKey(s => s.MovieEpisodeId)
                   .HasPrincipalKey(me => me.MovieEpisodeId);

                cfg.Property(mg => mg.Episode)
                   .HasDefaultValue(0);

                cfg.Property(mg => mg.IsDelete)
                   .HasDefaultValue(false);
            });
            #endregion

            #region EflmComment
            modelBuilder.Entity<EflmComment>(cfg =>
            {
                cfg.HasKey(c => new { c.CommentId });

                cfg.Property(c => c.CommentId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.Property(c => c.IsDelete)
                   .HasDefaultValue(false);
            });
            #endregion

            #region EflmProcessing
            modelBuilder.Entity<EflmProcessing>(cfg =>
            {
                cfg.HasKey(p => p.ProcessingId);

                cfg.Property(p => p.ProcessingId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.Property(p => p.IsDelete)
                   .HasDefaultValue(false);
            });
            #endregion

            #region EflmSubtitle
            modelBuilder.Entity<EflmSubtitle>(cfg =>
            {
                cfg.HasKey(s => s.SubtitleId);

                cfg.Property(s => s.SubtitleId)
                    .HasValueGenerator<GuidValueGenerator>();

                cfg.Property(s => s.IsDelete)
                   .HasDefaultValue(false);
            });
            #endregion
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            var modified = ChangeTracker.Entries().Where(e => e.State == EntityState.Modified || e.State == EntityState.Added);

            foreach (EntityEntry item in modified)
            {
                var changedOrAddedItem = item.Entity as IDateTracking;
                if (changedOrAddedItem != null)
                {
                    if (item.State == EntityState.Added)
                    {
                        changedOrAddedItem.CreateDate = DateTime.UtcNow;
                        changedOrAddedItem.CreateUser = "EFlim-System";
                    }
                    changedOrAddedItem.UpdateDate = DateTime.UtcNow;
                    changedOrAddedItem.UpdateUser = "EFlim-System";
                }
            }
            return base.SaveChanges();
        }

        public async override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            var modified = ChangeTracker.Entries().Where(e => e.State == EntityState.Modified || e.State == EntityState.Added || e.State == EntityState.Deleted);
            foreach (EntityEntry item in modified)
            {
                var changedOrAddedItem = item.Entity as IDateTracking;
                if (changedOrAddedItem != null)
                {
                    if (item.State == EntityState.Added)
                    {
                        changedOrAddedItem.CreateDate = DateTime.UtcNow;
                        changedOrAddedItem.CreateUser = "EFlim-System";
                    }
                    changedOrAddedItem.UpdateDate = DateTime.UtcNow;
                    changedOrAddedItem.UpdateUser = "EFlim-System";
                }
            }
            return await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
        }
    }
}
