﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFilm.Data.EF.Migrations
{
    public partial class InitDb_v3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EFLM_Movie_EFLM_Director_DirectorId",
                table: "EFLM_Movie");

            migrationBuilder.DropIndex(
                name: "IX_EFLM_Movie_DirectorId",
                table: "EFLM_Movie");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EFLM_Comment",
                table: "EFLM_Comment");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ReleaseDate",
                table: "EFLM_Movie",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateOnly),
                oldType: "date");

            migrationBuilder.AddColumn<Guid>(
                name: "CommentId",
                table: "EFLM_Comment",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "Content",
                table: "EFLM_Comment",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EFLM_Comment",
                table: "EFLM_Comment",
                column: "CommentId");

            migrationBuilder.CreateTable(
                name: "EFLM_MovieDirector",
                columns: table => new
                {
                    MovieId = table.Column<Guid>(type: "uuid", nullable: false),
                    DirectorId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EFLM_MovieDirector", x => new { x.MovieId, x.DirectorId });
                    table.ForeignKey(
                        name: "FK_EFLM_MovieDirector_EFLM_Director_DirectorId",
                        column: x => x.DirectorId,
                        principalTable: "EFLM_Director",
                        principalColumn: "DirectorId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EFLM_MovieDirector_EFLM_Movie_MovieId",
                        column: x => x.MovieId,
                        principalTable: "EFLM_Movie",
                        principalColumn: "MovieId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EFLM_Comment_UserId",
                table: "EFLM_Comment",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_EFLM_MovieDirector_DirectorId",
                table: "EFLM_MovieDirector",
                column: "DirectorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EFLM_MovieDirector");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EFLM_Comment",
                table: "EFLM_Comment");

            migrationBuilder.DropIndex(
                name: "IX_EFLM_Comment_UserId",
                table: "EFLM_Comment");

            migrationBuilder.DropColumn(
                name: "CommentId",
                table: "EFLM_Comment");

            migrationBuilder.DropColumn(
                name: "Content",
                table: "EFLM_Comment");

            migrationBuilder.AlterColumn<DateOnly>(
                name: "ReleaseDate",
                table: "EFLM_Movie",
                type: "date",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EFLM_Comment",
                table: "EFLM_Comment",
                columns: new[] { "UserId", "MovieEpisodeId" });

            migrationBuilder.CreateIndex(
                name: "IX_EFLM_Movie_DirectorId",
                table: "EFLM_Movie",
                column: "DirectorId");

            migrationBuilder.AddForeignKey(
                name: "FK_EFLM_Movie_EFLM_Director_DirectorId",
                table: "EFLM_Movie",
                column: "DirectorId",
                principalTable: "EFLM_Director",
                principalColumn: "DirectorId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
