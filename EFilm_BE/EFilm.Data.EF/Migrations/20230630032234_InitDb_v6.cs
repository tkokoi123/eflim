﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFilm.Data.EF.Migrations
{
    public partial class InitDb_v6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EFLM_Subtitle_EFLM_Movie_MovieId",
                table: "EFLM_Subtitle");

            migrationBuilder.RenameColumn(
                name: "MovieId",
                table: "EFLM_Subtitle",
                newName: "MovieEpisodeId");

            migrationBuilder.RenameIndex(
                name: "IX_EFLM_Subtitle_MovieId",
                table: "EFLM_Subtitle",
                newName: "IX_EFLM_Subtitle_MovieEpisodeId");

            migrationBuilder.AddForeignKey(
                name: "FK_EFLM_Subtitle_EFLM_MovieEpisode_MovieEpisodeId",
                table: "EFLM_Subtitle",
                column: "MovieEpisodeId",
                principalTable: "EFLM_MovieEpisode",
                principalColumn: "MovieEpisodeId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EFLM_Subtitle_EFLM_MovieEpisode_MovieEpisodeId",
                table: "EFLM_Subtitle");

            migrationBuilder.RenameColumn(
                name: "MovieEpisodeId",
                table: "EFLM_Subtitle",
                newName: "MovieId");

            migrationBuilder.RenameIndex(
                name: "IX_EFLM_Subtitle_MovieEpisodeId",
                table: "EFLM_Subtitle",
                newName: "IX_EFLM_Subtitle_MovieId");

            migrationBuilder.AddForeignKey(
                name: "FK_EFLM_Subtitle_EFLM_Movie_MovieId",
                table: "EFLM_Subtitle",
                column: "MovieId",
                principalTable: "EFLM_Movie",
                principalColumn: "MovieId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
