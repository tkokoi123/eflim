﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFilm.Data.EF.Migrations
{
    public partial class InitDb_v2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EFLM_Actor",
                columns: table => new
                {
                    ActorId = table.Column<Guid>(type: "uuid", nullable: false),
                    ActorName = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EFLM_Actor", x => x.ActorId);
                });

            migrationBuilder.CreateTable(
                name: "EFLM_Country",
                columns: table => new
                {
                    CountryId = table.Column<Guid>(type: "uuid", nullable: false),
                    CountryName = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    CountryCode = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EFLM_Country", x => x.CountryId);
                });

            migrationBuilder.CreateTable(
                name: "EFLM_Director",
                columns: table => new
                {
                    DirectorId = table.Column<Guid>(type: "uuid", nullable: false),
                    DirectorName = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EFLM_Director", x => x.DirectorId);
                });

            migrationBuilder.CreateTable(
                name: "EFLM_Genre",
                columns: table => new
                {
                    GenreId = table.Column<Guid>(type: "uuid", nullable: false),
                    GenreName = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EFLM_Genre", x => x.GenreId);
                });

            migrationBuilder.CreateTable(
                name: "EFLM_Tag",
                columns: table => new
                {
                    TagId = table.Column<Guid>(type: "uuid", nullable: false),
                    TagName = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EFLM_Tag", x => x.TagId);
                });

            migrationBuilder.CreateTable(
                name: "EFLM_Movie",
                columns: table => new
                {
                    MovieId = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    Overview = table.Column<string>(type: "text", nullable: false),
                    ReleaseDate = table.Column<DateOnly>(type: "date", nullable: false),
                    Showtimes = table.Column<string>(type: "text", nullable: false),
                    VoteAvg = table.Column<double>(type: "double precision", nullable: false),
                    VoteCount = table.Column<int>(type: "integer", nullable: false),
                    DirectorId = table.Column<Guid>(type: "uuid", nullable: false),
                    CountryId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EFLM_Movie", x => x.MovieId);
                    table.ForeignKey(
                        name: "FK_EFLM_Movie_EFLM_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "EFLM_Country",
                        principalColumn: "CountryId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EFLM_Movie_EFLM_Director_DirectorId",
                        column: x => x.DirectorId,
                        principalTable: "EFLM_Director",
                        principalColumn: "DirectorId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EFLM_MovieActor",
                columns: table => new
                {
                    MovieId = table.Column<Guid>(type: "uuid", nullable: false),
                    ActorId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EFLM_MovieActor", x => new { x.MovieId, x.ActorId });
                    table.ForeignKey(
                        name: "FK_EFLM_MovieActor_EFLM_Actor_ActorId",
                        column: x => x.ActorId,
                        principalTable: "EFLM_Actor",
                        principalColumn: "ActorId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EFLM_MovieActor_EFLM_Movie_MovieId",
                        column: x => x.MovieId,
                        principalTable: "EFLM_Movie",
                        principalColumn: "MovieId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EFLM_MovieEpisode",
                columns: table => new
                {
                    MovieEpisodeId = table.Column<Guid>(type: "uuid", nullable: false),
                    MovieId = table.Column<Guid>(type: "uuid", nullable: false),
                    ThumbnailUrl = table.Column<string>(type: "text", nullable: false),
                    ObjectUrl = table.Column<string>(type: "text", nullable: false),
                    Episode = table.Column<int>(type: "integer", nullable: false, defaultValue: 0),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EFLM_MovieEpisode", x => x.MovieEpisodeId);
                    table.ForeignKey(
                        name: "FK_EFLM_MovieEpisode_EFLM_Movie_MovieId",
                        column: x => x.MovieId,
                        principalTable: "EFLM_Movie",
                        principalColumn: "MovieId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EFLM_MovieGenre",
                columns: table => new
                {
                    GenreId = table.Column<Guid>(type: "uuid", nullable: false),
                    MovieId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EFLM_MovieGenre", x => new { x.GenreId, x.MovieId });
                    table.ForeignKey(
                        name: "FK_EFLM_MovieGenre_EFLM_Genre_GenreId",
                        column: x => x.GenreId,
                        principalTable: "EFLM_Genre",
                        principalColumn: "GenreId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EFLM_MovieGenre_EFLM_Movie_MovieId",
                        column: x => x.MovieId,
                        principalTable: "EFLM_Movie",
                        principalColumn: "MovieId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EFLM_MovieTag",
                columns: table => new
                {
                    MovieId = table.Column<Guid>(type: "uuid", nullable: false),
                    TagId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EFLM_MovieTag", x => new { x.MovieId, x.TagId });
                    table.ForeignKey(
                        name: "FK_EFLM_MovieTag_EFLM_Movie_MovieId",
                        column: x => x.MovieId,
                        principalTable: "EFLM_Movie",
                        principalColumn: "MovieId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EFLM_MovieTag_EFLM_Tag_TagId",
                        column: x => x.TagId,
                        principalTable: "EFLM_Tag",
                        principalColumn: "TagId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EFLM_Subtitle",
                columns: table => new
                {
                    SubtitleId = table.Column<Guid>(type: "uuid", nullable: false),
                    MovieId = table.Column<Guid>(type: "uuid", nullable: false),
                    SubName = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    Region = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    Url = table.Column<string>(type: "text", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EFLM_Subtitle", x => x.SubtitleId);
                    table.ForeignKey(
                        name: "FK_EFLM_Subtitle_EFLM_Movie_MovieId",
                        column: x => x.MovieId,
                        principalTable: "EFLM_Movie",
                        principalColumn: "MovieId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EFLM_Comment",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    MovieEpisodeId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EFLM_Comment", x => new { x.UserId, x.MovieEpisodeId });
                    table.ForeignKey(
                        name: "FK_EFLM_Comment_EFLM_MovieEpisode_MovieEpisodeId",
                        column: x => x.MovieEpisodeId,
                        principalTable: "EFLM_MovieEpisode",
                        principalColumn: "MovieEpisodeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EFLM_Comment_EFLM_User_UserId",
                        column: x => x.UserId,
                        principalTable: "EFLM_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EFLM_Processing",
                columns: table => new
                {
                    ProcessingId = table.Column<Guid>(type: "uuid", nullable: false),
                    MovieEpisodeId = table.Column<Guid>(type: "uuid", nullable: false),
                    Status = table.Column<short>(type: "smallint", nullable: false),
                    StartTime = table.Column<TimeOnly>(type: "time without time zone", nullable: false),
                    FinishTime = table.Column<TimeOnly>(type: "time without time zone", nullable: false),
                    Message = table.Column<string>(type: "text", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EFLM_Processing", x => x.ProcessingId);
                    table.ForeignKey(
                        name: "FK_EFLM_Processing_EFLM_MovieEpisode_MovieEpisodeId",
                        column: x => x.MovieEpisodeId,
                        principalTable: "EFLM_MovieEpisode",
                        principalColumn: "MovieEpisodeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EFLM_Comment_MovieEpisodeId",
                table: "EFLM_Comment",
                column: "MovieEpisodeId");

            migrationBuilder.CreateIndex(
                name: "IX_EFLM_Movie_CountryId",
                table: "EFLM_Movie",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_EFLM_Movie_DirectorId",
                table: "EFLM_Movie",
                column: "DirectorId");

            migrationBuilder.CreateIndex(
                name: "IX_EFLM_MovieActor_ActorId",
                table: "EFLM_MovieActor",
                column: "ActorId");

            migrationBuilder.CreateIndex(
                name: "IX_EFLM_MovieEpisode_MovieId",
                table: "EFLM_MovieEpisode",
                column: "MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_EFLM_MovieGenre_MovieId",
                table: "EFLM_MovieGenre",
                column: "MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_EFLM_MovieTag_TagId",
                table: "EFLM_MovieTag",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_EFLM_Processing_MovieEpisodeId",
                table: "EFLM_Processing",
                column: "MovieEpisodeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_EFLM_Subtitle_MovieId",
                table: "EFLM_Subtitle",
                column: "MovieId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EFLM_Comment");

            migrationBuilder.DropTable(
                name: "EFLM_MovieActor");

            migrationBuilder.DropTable(
                name: "EFLM_MovieGenre");

            migrationBuilder.DropTable(
                name: "EFLM_MovieTag");

            migrationBuilder.DropTable(
                name: "EFLM_Processing");

            migrationBuilder.DropTable(
                name: "EFLM_Subtitle");

            migrationBuilder.DropTable(
                name: "EFLM_Actor");

            migrationBuilder.DropTable(
                name: "EFLM_Genre");

            migrationBuilder.DropTable(
                name: "EFLM_Tag");

            migrationBuilder.DropTable(
                name: "EFLM_MovieEpisode");

            migrationBuilder.DropTable(
                name: "EFLM_Movie");

            migrationBuilder.DropTable(
                name: "EFLM_Country");

            migrationBuilder.DropTable(
                name: "EFLM_Director");
        }
    }
}
