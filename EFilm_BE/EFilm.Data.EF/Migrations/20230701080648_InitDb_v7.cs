﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFilm.Data.EF.Migrations
{
    public partial class InitDb_v7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TypeMovie",
                table: "EFLM_Movie",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TypeSubtitle",
                table: "EFLM_Movie",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TypeMovie",
                table: "EFLM_Movie");

            migrationBuilder.DropColumn(
                name: "TypeSubtitle",
                table: "EFLM_Movie");
        }
    }
}
