﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFilm.Data.EF.Migrations
{
    public partial class Update_EflmUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "EFLM_User");

            migrationBuilder.RenameColumn(
                name: "Createdate",
                table: "EFLM_User",
                newName: "CreateDate");

            migrationBuilder.AlterColumn<short>(
                name: "Status",
                table: "EFLM_User",
                type: "smallint",
                nullable: false,
                defaultValue: (short)1,
                oldClrType: typeof(short),
                oldType: "smallint");

            migrationBuilder.AddColumn<short>(
                name: "Role",
                table: "EFLM_User",
                type: "smallint",
                nullable: false,
                defaultValue: (short)2);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Role",
                table: "EFLM_User");

            migrationBuilder.RenameColumn(
                name: "CreateDate",
                table: "EFLM_User",
                newName: "Createdate");

            migrationBuilder.AlterColumn<short>(
                name: "Status",
                table: "EFLM_User",
                type: "smallint",
                nullable: false,
                oldClrType: typeof(short),
                oldType: "smallint",
                oldDefaultValue: (short)1);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "EFLM_User",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
