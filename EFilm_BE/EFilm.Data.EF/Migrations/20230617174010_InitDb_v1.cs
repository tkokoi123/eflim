﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFilm.Data.EF.Migrations
{
    public partial class InitDb_v1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EFLM_User",
                columns: table => new
                {
                    Username = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    Email = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    Password = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    Fullname = table.Column<string>(type: "character varying(1000)", maxLength: 1000, nullable: false),
                    Mobile = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    Createdate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Status = table.Column<short>(type: "smallint", nullable: false),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateUser = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EFLM_User", x => new { x.Username, x.Email });
                    table.UniqueConstraint("AK_EFLM_User_UserId", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "EFLM_Token",
                columns: table => new
                {
                    Token = table.Column<string>(type: "text", nullable: false),
                    UserId = table.Column<Guid>(type: "uuid", nullable: false),
                    ExpriedDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EFLM_Token", x => x.Token);
                    table.ForeignKey(
                        name: "FK_EFLM_Token_EFLM_User_UserId",
                        column: x => x.UserId,
                        principalTable: "EFLM_User",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EFLM_Token_UserId",
                table: "EFLM_Token",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_EFLM_Token_UserId_Token",
                table: "EFLM_Token",
                columns: new[] { "UserId", "Token" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_EFLM_User_Username_Email_UserId",
                table: "EFLM_User",
                columns: new[] { "Username", "Email", "UserId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EFLM_Token");

            migrationBuilder.DropTable(
                name: "EFLM_User");
        }
    }
}
