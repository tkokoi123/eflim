﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFilm.Data.EF.Migrations
{
    public partial class InitDb_v8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Region",
                table: "EFLM_Subtitle");

            migrationBuilder.DropColumn(
                name: "Url",
                table: "EFLM_Subtitle");

            migrationBuilder.DropColumn(
                name: "DirectorId",
                table: "EFLM_Movie");

            migrationBuilder.RenameColumn(
                name: "SubName",
                table: "EFLM_Subtitle",
                newName: "FolderName");

            migrationBuilder.AddColumn<string>(
                name: "Language",
                table: "EFLM_Subtitle",
                type: "character varying(30)",
                maxLength: 30,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Language",
                table: "EFLM_Subtitle");

            migrationBuilder.RenameColumn(
                name: "FolderName",
                table: "EFLM_Subtitle",
                newName: "SubName");

            migrationBuilder.AddColumn<string>(
                name: "Region",
                table: "EFLM_Subtitle",
                type: "character varying(255)",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Url",
                table: "EFLM_Subtitle",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "DirectorId",
                table: "EFLM_Movie",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }
    }
}
