﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFilm.Data.EF.Migrations
{
    public partial class InitDb_v5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EFLM_Comment_EFLM_MovieEpisode_MovieEpisodeId",
                table: "EFLM_Comment");

            migrationBuilder.RenameColumn(
                name: "MovieEpisodeId",
                table: "EFLM_Comment",
                newName: "MovieId");

            migrationBuilder.RenameIndex(
                name: "IX_EFLM_Comment_MovieEpisodeId",
                table: "EFLM_Comment",
                newName: "IX_EFLM_Comment_MovieId");

            migrationBuilder.AddForeignKey(
                name: "FK_EFLM_Comment_EFLM_Movie_MovieId",
                table: "EFLM_Comment",
                column: "MovieId",
                principalTable: "EFLM_Movie",
                principalColumn: "MovieId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EFLM_Comment_EFLM_Movie_MovieId",
                table: "EFLM_Comment");

            migrationBuilder.RenameColumn(
                name: "MovieId",
                table: "EFLM_Comment",
                newName: "MovieEpisodeId");

            migrationBuilder.RenameIndex(
                name: "IX_EFLM_Comment_MovieId",
                table: "EFLM_Comment",
                newName: "IX_EFLM_Comment_MovieEpisodeId");

            migrationBuilder.AddForeignKey(
                name: "FK_EFLM_Comment_EFLM_MovieEpisode_MovieEpisodeId",
                table: "EFLM_Comment",
                column: "MovieEpisodeId",
                principalTable: "EFLM_MovieEpisode",
                principalColumn: "MovieEpisodeId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
