﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFilm.Data.EF.Migrations
{
    public partial class InitDb_v9 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "HotMovie",
                table: "EFLM_Movie",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HotMovie",
                table: "EFLM_Movie");
        }
    }
}
