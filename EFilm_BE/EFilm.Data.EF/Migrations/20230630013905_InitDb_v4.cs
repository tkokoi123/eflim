﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EFilm.Data.EF.Migrations
{
    public partial class InitDb_v4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ThumbnailUrl",
                table: "EFLM_MovieEpisode");

            migrationBuilder.DropColumn(
                name: "CountryCode",
                table: "EFLM_Country");

            migrationBuilder.AddColumn<string>(
                name: "ThumbnailUrl",
                table: "EFLM_Movie",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ThumbnailUrl",
                table: "EFLM_Movie");

            migrationBuilder.AddColumn<string>(
                name: "ThumbnailUrl",
                table: "EFLM_MovieEpisode",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "CountryCode",
                table: "EFLM_Country",
                type: "character varying(10)",
                maxLength: 10,
                nullable: false,
                defaultValue: "");
        }
    }
}
