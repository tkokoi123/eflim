﻿using EFilm.Data.Dtos.Requests;
using EFilm.Data.Entities;
using EFilm.Infrastructure.Configurations;
using EFilm.Infrastructure.Constants;
using Microsoft.EntityFrameworkCore;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public class EflmMovieRepository : BaseRepository<EflmMovie>, IEflmMovieRepository
    {
        public EflmMovieRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<EflmMovie>> GetListMovieByListMovieId(IEnumerable<Guid> movieIds)
        {
            return await FindMultiple(m => movieIds.Contains(m.MovieId));
        }

        public async Task<IEnumerable<EflmMovie>> GetListMovieIncludeMovieGenres()
        {
            var movies = await _context.Set<EflmMovie>().Include(m => m.MovieGenres).Include(m => m.MovieEpisodes).ToListAsync().ConfigureAwait(false);
            return movies;
        }

        public async Task<EflmMovie> GetDetailsMovie(Guid movieId)
        {

            var result = await _context.Set<EflmMovie>()
                                     .Include(m => m.Country)
                                     .Include(m => m.MovieDirectors).ThenInclude(md => md.Director)
                                     .Include(m => m.MovieTags).ThenInclude(md => md.Tag)
                                     .Include(m => m.MovieActors).ThenInclude(md => md.Actor)
                                     .Include(m => m.MovieGenres).ThenInclude(md => md.Genre)
                                     .Include(m => m.MovieEpisodes)
                                     .Include(m => m.Comments).IgnoreAutoIncludes()
                                     .AsNoTracking()
                                     .FirstOrDefaultAsync(m => m.MovieId.Equals(movieId))
                                     .ConfigureAwait(false);

            return result;

        }

        public async Task<List<EflmMovie>> GetMoviePaging(int currentPage)
        {
            int pageSize = AppSettings.PageSize;

            var result = await _context.Set<EflmMovie>()
                .AsNoTracking()
                .Skip(currentPage * pageSize)
                .Take(pageSize)
                .OrderBy(f => f.CreateDate)
                .ToListAsync()
                .ConfigureAwait(false);

            return result;
        }

        public async Task<IEnumerable<EflmMovie>> GetHotMovies()
        {
            var result = await _context.Set<EflmMovie>()
                                .Include(m=>m.MovieEpisodes)
                                .Where(m => m.HotMovie.Equals(true))
                                .Take(Common.NumberHotMovieDisplayHomePage)
                                .OrderByDescending(m => m.CreateDate).ToListAsync().ConfigureAwait(false);
            return result;
        }

        public async Task<IEnumerable<EflmMovie>> GetNewMovieByTypeMovie(TypeMovie typeMovie)
        {
            var result = await _context.Set<EflmMovie>()
                                .Include(m => m.MovieEpisodes)
                                .Where(m => m.TypeMovie.Equals(typeMovie))
                                .Take(Common.NumberMovieDisplayHomePage)
                                .OrderByDescending(m => m.CreateDate).ToListAsync().ConfigureAwait(false);
            return result;
        }
    }
}
