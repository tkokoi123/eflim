﻿using EFilm.Data.Dtos.Requests;
using EFilm.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public class EflmMovieEpisodeRepository : BaseRepository<EflmMovieEpisode>, IEflmMovieEpisodeRepository
    {
        public EflmMovieEpisodeRepository(AppDbContext context) : base(context)
        {
        }
    }
}
