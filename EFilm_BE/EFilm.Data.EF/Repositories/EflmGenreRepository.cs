﻿using EFilm.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public class EflmGenreRepository : BaseRepository<EflmGenre>, IEflmGenreRepository
    {
        public EflmGenreRepository(AppDbContext context) : base(context)
        {

        }
        public async Task<IEnumerable<EflmGenre>> GetGenresByMovieId(Guid movieId)
        {
            var genIds = await _context.Set<EflmMovieGenre>()
                .Where(mg => mg.MovieId.Equals(movieId))
                .Select(mg => mg.GenreId)
                .ToListAsync().ConfigureAwait(false);
            return await _context.Set<EflmGenre>()
                .Where(t => genIds.Contains(t.GenreId))
                .ToListAsync().ConfigureAwait(false);
        }

    }
}
