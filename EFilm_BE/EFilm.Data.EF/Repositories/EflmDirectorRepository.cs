﻿using EFilm.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public class EflmDirectorRepository : BaseRepository<EflmDirector>, IEflmDirectorRepository
    {
        public EflmDirectorRepository(AppDbContext context) : base(context)
        {
            
        }
        public async Task<IEnumerable<EflmDirector>> GetDirectorsByMovieId(Guid movieId)
        {
            var directorIds =await _context.Set<EflmMovieDirector>()
                .Where(md=>md.MovieId.Equals(movieId))
                .Select(md=>md.DirectorId)
                .ToListAsync().ConfigureAwait(false);
            var result =  await _context.Set<EflmDirector>()
                .Where(d => directorIds.Contains(d.DirectorId))
                .ToListAsync().ConfigureAwait(false);
            return result; 
        }

    }
}
