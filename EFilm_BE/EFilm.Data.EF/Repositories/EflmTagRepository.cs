﻿using EFilm.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public class EflmTagRepository : BaseRepository<EflmTag>, IEflmTagRepository
    {
        public EflmTagRepository(AppDbContext context) : base(context)
        {

        }
        public async Task<IEnumerable<EflmTag>> SearchTagByName(string tagName)
        {
            var result = await FindMultiple(t => t.TagName.ToUpper().Contains(tagName.ToUpper()));
            return result;
        }
        public async Task<IEnumerable<EflmTag>> GetTagsByMovieId(Guid movieId)
        {
            var tagIds = await _context.Set<EflmMovieTag>()
                .Where(mt => mt.MovieId.Equals(movieId))
                .Select(mt => mt.TagId)
                .ToListAsync().ConfigureAwait(false);
            return await _context.Set<EflmTag>()
                .Where(t => tagIds.Contains(t.TagId))
                .ToListAsync().ConfigureAwait(false);
        }

    }
}
