﻿using EFilm.Data.Dtos.Requests;
using EFilm.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public class EflmCountryRepository : BaseRepository<EflmCountry>, IEflmCountryRepository
    {
        public EflmCountryRepository(AppDbContext context) : base(context)
        {
        }
        public async Task<EflmCountry> GetCountryByCountryId(Guid countryId)
        {
           var result = await _context.Set<EflmCountry>()
                .AsNoTracking()
                .FirstOrDefaultAsync(c=>c.CountryId.Equals(countryId))
                .ConfigureAwait(false);
            return result;
        }
        
    }
}
