﻿using EFilm.Data.Entities;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public interface IEflmTagRepository : IBaseRepository<EflmTag>
    {
        public Task<IEnumerable<EflmTag>> SearchTagByName(string tagName);
        public Task<IEnumerable<EflmTag>> GetTagsByMovieId(Guid movieId);
    }
}
