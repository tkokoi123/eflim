﻿using EFilm.Data.Entities;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public interface IEflmCountryRepository : IBaseRepository<EflmCountry>
    {
        public Task<EflmCountry> GetCountryByCountryId(Guid countryId);
    }
}
