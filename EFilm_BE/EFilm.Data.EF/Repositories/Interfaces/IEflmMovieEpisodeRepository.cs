﻿using EFilm.Data.Entities;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public interface IEflmMovieEpisodeRepository : IBaseRepository<EflmMovieEpisode>
    {
      
    }
}
