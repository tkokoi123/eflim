﻿using EFilm.Data.Entities;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public interface IEflmUserRepository : IBaseRepository<EflmUser>
    {
    }
}
