﻿using EFilm.Data.Entities;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public interface IEflmActorRepository : IBaseRepository<EflmActor>
    {
        public Task<IEnumerable<EflmActor>> GetActorsByMovieId(Guid movieId);
    }
}
