﻿using EFilm.Data.Entities;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public interface IEflmCommentRepository : IBaseRepository<EflmComment>
    {
      
    }
}
