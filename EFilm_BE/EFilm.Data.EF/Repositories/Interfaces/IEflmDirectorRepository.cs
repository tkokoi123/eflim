﻿using EFilm.Data.Entities;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public interface IEflmDirectorRepository : IBaseRepository<EflmDirector>
    {
        public Task<IEnumerable<EflmDirector>> GetDirectorsByMovieId(Guid movieId);
    }
}
