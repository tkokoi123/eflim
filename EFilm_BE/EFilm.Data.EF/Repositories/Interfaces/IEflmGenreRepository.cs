﻿using EFilm.Data.Entities;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public interface IEflmGenreRepository : IBaseRepository<EflmGenre>
    {
        public Task<IEnumerable<EflmGenre>> GetGenresByMovieId(Guid movieId);
    }
}
