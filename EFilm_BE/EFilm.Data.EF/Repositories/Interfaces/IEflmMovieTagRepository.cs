﻿using EFilm.Data.Entities;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public interface IEflmMovieTagRepository : IBaseRepository<EflmMovieTag>
    {
        public Task<IEnumerable<Guid>> GetListMovieIdByListTag(IEnumerable<EflmTag> tags);
    }
}
