﻿using EFilm.Data.Entities;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public interface IEflmMovieRepository : IBaseRepository<EflmMovie>
    {
        public Task<IEnumerable<EflmMovie>> GetListMovieByListMovieId(IEnumerable<Guid> movieIds);
        public Task<IEnumerable<EflmMovie>> GetListMovieIncludeMovieGenres();
        public Task<EflmMovie> GetDetailsMovie(Guid movieId);
        public Task<IEnumerable<EflmMovie>> GetHotMovies();
        public Task<IEnumerable<EflmMovie>> GetNewMovieByTypeMovie(TypeMovie typeMovie);
        public Task<List<EflmMovie>> GetMoviePaging(int currentPage);
    }
}
