﻿using EFilm.Data.Entities;
using Microsoft.EntityFrameworkCore;
namespace EFilm.Data.EF.Repositories.Interfaces
{
    public class EflmMovieTagRepository : BaseRepository<EflmMovieTag>, IEflmMovieTagRepository
    {
        public EflmMovieTagRepository(AppDbContext context) : base(context)
        {
        }
        public async Task<IEnumerable<Guid>> GetListMovieIdByListTag(IEnumerable<EflmTag> tags)
        {
            var result = await _context.Set<EflmMovieTag>()
              .Where(mt => tags.Select(t => t.TagId)
              .Contains (mt.TagId))
              .Select(mt => mt.MovieId).ToListAsync().ConfigureAwait(false);
            return result;
        }
    }
}
