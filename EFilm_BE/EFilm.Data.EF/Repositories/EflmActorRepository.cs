﻿using EFilm.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public class EflmActorRepository : BaseRepository<EflmActor>, IEflmActorRepository
    {
        public EflmActorRepository(AppDbContext context) : base(context)
        {
            
        }
        public async Task<IEnumerable<EflmActor>> GetActorsByMovieId(Guid movieId)
        {
            var actorIds =await _context.Set<EflmMovieActor>()
                .Where(ma=> ma.MovieId.Equals(movieId))
                .Select(ma => ma.ActorId)
                .ToListAsync().ConfigureAwait(false);
            return await _context.Set<EflmActor>()
                .Where(a => actorIds.Contains(a.ActorId))
                .ToListAsync().ConfigureAwait(false);
        }
    }
}
