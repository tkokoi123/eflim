﻿using EFilm.Data.EF.Repositories.Interfaces;
using EFilm.Data.Entities;

namespace EFilm.Data.EF.Repositories
{
    public class EflmUserRepository : BaseRepository<EflmUser>, IEflmUserRepository
    {
        public EflmUserRepository(AppDbContext context) : base(context)
        {
        }


    }
}
