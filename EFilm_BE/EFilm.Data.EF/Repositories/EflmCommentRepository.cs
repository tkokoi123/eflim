﻿using EFilm.Data.Dtos.Requests;
using EFilm.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public class EflmCommentRepository : BaseRepository<EflmComment>, IEflmCommentRepository
    {
        public EflmCommentRepository(AppDbContext context) : base(context)
        {
        }
    }
}
