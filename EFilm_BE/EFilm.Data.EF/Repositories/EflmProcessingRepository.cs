﻿using EFilm.Data.Entities;

namespace EFilm.Data.EF.Repositories.Interfaces
{
    public class EflmProcessingRepository : BaseRepository<EflmProcessing>, IEflmProcessingRepository
    {
        public EflmProcessingRepository(AppDbContext context) : base(context)
        {

        }

    }
}
