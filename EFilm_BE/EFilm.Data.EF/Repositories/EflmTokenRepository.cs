﻿using EFilm.Data.EF.Repositories.Interfaces;
using EFilm.Data.Entities;

namespace EFilm.Data.EF.Repositories
{
    public class EflmTokenRepository : BaseRepository<EflmToken>, IEflmTokenRepository
    {
        public EflmTokenRepository(AppDbContext context) : base(context)
        {
        }
    }
}
