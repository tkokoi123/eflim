﻿namespace EFilm.Infrastructure.Enums
{
    public static class SystemEnum
    {
        public enum UserStatus
        {
            Activated = 1,
            Blocked = 2
        }

        public enum UserRoles
        {
            Admin = 1,
            User = 2,
        }

        public enum ProcessStatus
        {
            Error = -1,
            Pending = 0,
            Executing = 1,
            Done = 2,
            Cancel = 3
        }

    }
}
