﻿using AutoMapper;
using EFilm.Data.Dtos.Responses;
using EFilm.Data.Entities;
using System.Globalization;

namespace EFilm.Application.AutoMapper
{
    public class DomainToResponseModelMappingProfile : Profile
    {
        public DomainToResponseModelMappingProfile()
        {
            CreateMap<OmdbapiDetailsModel, OmdbapiDetailsResponseModel>()
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
                .ForMember(dest => dest.Overview, opt => opt.MapFrom(src => src.Plot))
                .ForMember(dest => dest.ReleasedDate, opt => opt.MapFrom(src => DateTime.ParseExact(src.Released, "dd MMM yyyy", CultureInfo.InvariantCulture)))
                .ForMember(dest => dest.Showtimes, opt => opt.MapFrom(src => src.Runtime))
                .ForMember(dest => dest.Directors, opt => opt.MapFrom(src => ToWordsList(src.Director)))
                .ForMember(dest => dest.Actors, opt => opt.MapFrom(src => ToWordsList(src.Actors)))
                .ForMember(dest => dest.Genres, opt => opt.MapFrom(src => ToWordsList(src.Genre)));

            CreateMap<EflmMovie, ListMovieResponseModel>();
            CreateMap<Search, ListMovieOmdbapiResponseModel>();
            CreateMap<EflmMovie, MovieDetailsResponseModel>()
                .ForMember(dest => dest.ReleaseYear, opt => opt.MapFrom(src => src.ReleaseDate.Year))
                .ForMember(dest => dest.Directors, opt => opt.MapFrom(src => src.MovieDirectors.Select(md => md.Director)))
                .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => src.MovieTags.Select(md => md.Tag)))
                .ForMember(dest => dest.Actors, opt => opt.MapFrom(src => src.MovieActors.Select(md => md.Actor)))
                .ForMember(dest => dest.Genres, opt => opt.MapFrom(src => src.MovieGenres.Select(md => md.Genre)))
                .ForMember(dest => dest.Episodes, opt => opt.MapFrom(src => src.MovieEpisodes));

            CreateMap<EflmComment, CommentResponseModel>();
            CreateMap<EflmCountry, CountryResponseModel>();
            CreateMap<EflmMovieEpisode, MovieEpisodeResponseModel>();
            CreateMap<EflmTag, TagResponseModel>();
            CreateMap<EflmActor, ActorResponseModel>();
            CreateMap<EflmGenre, GenreResponseModel>();
            CreateMap<EflmDirector, DirectorResponseModel>();
            CreateMap<EflmMovie, MovieDisplayResponseModel>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom
                (src => (src.TypeSubtitle == TypeSubtitle.Subtitle ? "Vietsub" : "Thuyết Minh")
                + (src.MovieEpisodes.Max(e => e.Episode) == 0 ? "" : " Tập " + src.MovieEpisodes.Max(e => e.Episode))));
            CreateMap<EflmMovie, AdminMovieResponseModel>()
                .ForMember(dest => dest.MovieType, opt => opt.MapFrom(src => src.TypeMovie));

        }
        private static List<string> ToWordsList(string words)
        {
            return string.IsNullOrWhiteSpace(words) ? new List<string>() : words.Split(",").ToList();
        }
    }
}
