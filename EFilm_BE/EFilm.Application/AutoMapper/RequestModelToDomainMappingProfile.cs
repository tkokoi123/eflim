﻿using AutoMapper;
using EFilm.Data.Dtos.Requests;
using EFilm.Data.Dtos.Responses;
using EFilm.Data.Entities;

namespace EFilm.Application.AutoMapper
{
    public class RequestModelToDomainMappingProfile : Profile
    {
        public RequestModelToDomainMappingProfile()
        {
            CreateMap<UserRegisterRequestModel, EflmUser>();
        }
    }
}
