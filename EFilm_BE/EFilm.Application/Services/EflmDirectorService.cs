﻿using AutoMapper;
using EFilm.Application.Services.Interfaces;
using EFilm.Data.Dtos.Responses;
using EFilm.Data.EF;
using Microsoft.Extensions.Logging;

namespace EFilm.Application.Services
{
    public class EflmDirectorService : IEflmDirectorService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<EflmDirectorService> _logger;
        private readonly IMapper _mapper;

        public EflmDirectorService(IUnitOfWork unitOfWork, ILogger<EflmDirectorService> logger, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<IEnumerable<DirectorResponseModel>> GetAll()
        {
            try
            {
                var data = await _unitOfWork.EflmDirectorRepository
                    .All()
                    .ConfigureAwait(false);

                var mappedData = _mapper.Map<IEnumerable<DirectorResponseModel>>(data);

                return mappedData;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return Enumerable.Empty<DirectorResponseModel>();
            }
        }

        public async Task<IEnumerable<ActorResponseModel>> GetAllActor()
        {
            try
            {
                var data = await _unitOfWork.EflmActorRepository
                    .All()
                    .ConfigureAwait(false);

                var mappedData = _mapper.Map<IEnumerable<ActorResponseModel>>(data);

                return mappedData;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return Enumerable.Empty<ActorResponseModel>();
            }
        }
    }
}
