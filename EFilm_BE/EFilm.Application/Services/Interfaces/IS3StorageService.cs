﻿using EFilm.Data.S3Obj;

namespace EFilm.Application.Services.Interfaces
{
    public interface IS3StorageService
    {
        public Task<bool> UploadFileAsync(S3RequestData obj);
        public string GetFileUrl(S3RequestData obj);
        public Task<bool> UploadFolderAsync(S3RquestDirectory obj);
        public Task<Stream> DownloadFileContent(S3RquestDirectory obj);
        public Task<bool> DeleteDirectory(S3RquestDirectory obj);
    }
}
