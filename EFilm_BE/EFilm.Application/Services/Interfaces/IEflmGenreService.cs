﻿using EFilm.Data.Dtos.Responses;

namespace EFilm.Application.Services.Interfaces
{
    public interface IEflmGenreService
    {
        public Task<IEnumerable<GenreResponseModel>> GetGenres();
    }
}
