﻿using EFilm.Data.Dtos.Responses;

namespace EFilm.Application.Services.Interfaces
{
    public interface IEflmDirectorService
    {
        public Task<IEnumerable<DirectorResponseModel>> GetAll();
        public Task<IEnumerable<ActorResponseModel>> GetAllActor();
    }
}
