﻿using EFilm.Data.Entities;

namespace EFilm.Application.Services.Interfaces
{
    public interface IEflmProcessingService
    {
        public Task<bool> UpdateStatusProcesssing(EflmProcessing eflmProcessing);
        public Task<EflmProcessing> AddProcessing(EflmProcessing eflmProcessing);
        public Task<EflmProcessing> GetProcessingById(Guid processId);
    }
}
