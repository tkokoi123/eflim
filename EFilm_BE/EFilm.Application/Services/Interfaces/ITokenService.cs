﻿using System.Security.Claims;

namespace EFilm.Application.Services.Interfaces
{
    public interface ITokenService
    {
        public string GenerateAccessToken(IEnumerable<Claim> claims);
        public string GenerateRefreshToken();
        public ClaimsPrincipal GetPrincipalFromExpriedToken(string token);
    }
}
