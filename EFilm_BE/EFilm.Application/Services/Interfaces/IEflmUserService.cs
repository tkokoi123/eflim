﻿using EFilm.Data.Dtos.Requests;
using EFilm.Data.Entities;

namespace EFilm.Application.Services.Interfaces
{
    public interface IEflmUserService
    {
        public Task<EflmUser?> UserLogin(UserLoginRequestModel rqUser);

        public Task<bool> UserRegister(UserRegisterRequestModel rqUser);

        public Task<EflmUser?> FindUserByUsername(string? username);
    }
}
