﻿using EFilm.Data.Entities;

namespace EFilm.Application.Services.Interfaces
{
    public interface IEflmTokenService
    {
        public Task<bool> SaveRefeshTokenDb(Guid userId, string refreshToken, DateTime expiredTime);
        public Task<EflmToken> GetTokenInfo(Guid userId);
    }
}
