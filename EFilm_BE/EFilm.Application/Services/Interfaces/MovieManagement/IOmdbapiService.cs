﻿using EFilm.Data.Dtos.Responses;

namespace EFilm.Application.Services.MovieManagement
{
    public interface IOmdbapiService
    {
        //search list omdbapi by title
        public Task<IEnumerable<ListMovieOmdbapiResponseModel>?> SearchListMovieOmdbapi(string title);
        public Task<OmdbapiDetailsResponseModel?> GetMovieDetailOmdbapi(string id);

    }
}
