﻿using EFilm.Data.Dtos.Requests;
using EFilm.Data.Dtos.Responses;
using EFilm.Data.Entities;

namespace EFilm.Application.Services.Interfaces.MovieManagement
{
    public interface IEflmMovieService
    {
        public Task<IEnumerable<MovieDisplayResponseModel>> GetListMovieByTagName(string tagName);
        public Task<IEnumerable<MovieDisplayResponseModel>> FilterMovie(FilterMovieRequestModel filterRequest);
        public Task<MovieDetailsResponseModel> GetDetailsMovie(Guid movieId);
        public Task<IEnumerable<MovieDisplayResponseModel>> GetHotMovies();
        public Task<IEnumerable<MovieDisplayResponseModel>> GetNewMovieByTypeMovie(TypeMovie typeMovie);
        public Task<AdminListMovieResponseModel> GetMovieList(int currentPage);
        public Task<IEnumerable<MovieEpisodeResponseModel>> GetMovieResponseById(Guid movieId);
        public Task<Guid> AddFilmEpisode(Guid filmId, int episodeNum);
    }
}
