﻿using EFilm.Data.Dtos.Responses;

namespace EFilm.Application.Services.Interfaces
{
    public interface IEflmCountryService
    {
        public Task<IEnumerable<CountryResponseModel>> GetCountrys();
    }
}
