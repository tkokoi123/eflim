﻿using AutoMapper;
using EFilm.Application.Services.Interfaces;
using EFilm.Data.Dtos.Responses;
using EFilm.Data.EF;
using EFilm.Data.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFilm.Application.Services
{
    public class EflmGenreService : IEflmGenreService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<EflmGenreService> _logger;
        private readonly IMapper _mapper;

        public EflmGenreService(IUnitOfWork unitOfWork, ILogger<EflmGenreService> logger, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
        }
        public async Task<IEnumerable<GenreResponseModel>> GetGenres()
        {
            var genres = await _unitOfWork.EflmGenreRepository.All().ConfigureAwait(false);
            var result =_mapper.Map<IEnumerable<GenreResponseModel>>(genres);
            return result;
        }
    }
}
