﻿using AutoMapper;
using EFilm.Data.Dtos.Responses;
using EFilm.Data.EF;
using EFilm.Infrastructure.Configurations;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace EFilm.Application.Services.MovieManagement
{
    public class OmdbapiService : IOmdbapiService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<EflmUserService> _logger;
        private readonly IMapper _mapper;
        private readonly IHttpClientFactory _httpClientFactory;

        public OmdbapiService(IUnitOfWork unitOfWork, ILogger<EflmUserService> logger, IMapper mapper, IHttpClientFactory httpClientFactory)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
            _httpClientFactory = httpClientFactory;
        }

        //search list omdbapi by title
        public async Task<IEnumerable<ListMovieOmdbapiResponseModel>?> SearchListMovieOmdbapi(string title)
        {
            string url = string.Format("?apikey={0}&s={1}", AppSettings.OmdbKey, title);

            var result = await GetObjectInformationApi<SearchOmdbapiResponseModel>(url);
            if (result == null)
            {
                return null;
            }

            var listMovie = _mapper.Map<IEnumerable<ListMovieOmdbapiResponseModel>>(result.Search);
            return listMovie.Where(lm => lm.Type != TypeEnum.Game);
        }

        public async Task<OmdbapiDetailsResponseModel?> GetMovieDetailOmdbapi(string id)
        {
            string url = string.Format("?apikey={0}&i={1}&plot=full", AppSettings.OmdbKey, id);

            var result = await GetObjectInformationApi<OmdbapiDetailsModel>(url);
            if (result == null)
            {
                return null;
            }
            var movieDetail = _mapper.Map<OmdbapiDetailsResponseModel>(result);
            return movieDetail;
        }

        private async Task<T?> GetObjectInformationApi<T>(string url)
        {

            HttpClient client = _httpClientFactory.CreateClient(AppSettings.OmdbName);

            // List data response.
            HttpResponseMessage response = await client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body.
                var data = await response.Content.ReadAsStringAsync();
                //deserialize to your class
                var dataObjects = JsonConvert.DeserializeObject<T>(data);
                return dataObjects;
            }

            return default(T?);
        }

    }
}
