﻿using AutoMapper;
using EFilm.Application.Services.Interfaces.MovieManagement;
using EFilm.Data.Dtos.Requests;
using EFilm.Data.Dtos.Responses;
using EFilm.Data.EF;
using EFilm.Data.Entities;
using EFilm.Infrastructure.Helpers;
using Microsoft.Extensions.Logging;

namespace EFilm.Application.Services.MovieManagement
{
    public class EflmMovieService : IEflmMovieService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<EflmMovieService> _logger;
        private readonly IMapper _mapper;

        public EflmMovieService(IUnitOfWork unitOfWork, ILogger<EflmMovieService> logger, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<IEnumerable<MovieDisplayResponseModel>> GetListMovieByTagName(string tagName)
        {
            var tags = await _unitOfWork.EflmTagRepository.SearchTagByName(tagName);
            if (tags == null)
            {
                return null;
            }
            var movieId = await _unitOfWork.EflmMovieTagRepository.GetListMovieIdByListTag(tags);
            var movie = await _unitOfWork.EflmMovieRepository.GetListMovieByListMovieId(movieId);
            var result = _mapper.Map<IEnumerable<MovieDisplayResponseModel>>(movie);
            return result;
        }
        public async Task<IEnumerable<MovieDisplayResponseModel>> FilterMovie(FilterMovieRequestModel filterRequest)
        {
            var movies = await _unitOfWork.EflmMovieRepository.GetListMovieIncludeMovieGenres().ConfigureAwait(false);

            if (filterRequest.CountryId != default(Guid))
            {
                movies = movies.Where(m => m.CountryId.Equals(filterRequest.CountryId)).AsEnumerable();
            }

            if (filterRequest.GenreId != default(Guid))
            {
                movies = movies.Where(m => m.MovieGenres.Select(mg => mg.GenreId).Contains(filterRequest.GenreId)).AsEnumerable();
            }

            if (filterRequest.TypeMovie != TypeMovie.None)
            {
                movies = movies.Where(m => m.TypeMovie.Equals(filterRequest.TypeMovie)).AsEnumerable();
            }

            if (filterRequest.TypeSubtitle != TypeSubtitle.None)
            {
                movies = movies.Where(m => m.TypeSubtitle.Equals(filterRequest.TypeSubtitle)).AsEnumerable();
            }

            if (filterRequest.ReleaseYear != -1)
            {
                if (filterRequest.ReleaseYear > DateTime.Now.Year - 15)
                {
                    movies = movies.Where(m => m.ReleaseDate.Year.Equals(filterRequest.ReleaseYear)).AsEnumerable();
                }
                else
                {
                    movies = movies.Where(m => m.ReleaseDate.Year < (filterRequest.ReleaseYear)).AsEnumerable();
                }
            }

            var movieResponseModels = _mapper.Map<IEnumerable<MovieDisplayResponseModel>>(movies);
            return movieResponseModels;
        }

        public async Task<MovieDetailsResponseModel> GetDetailsMovie(Guid movieId)
        {
            var movieDetail = await _unitOfWork.EflmMovieRepository
                .GetDetailsMovie(movieId)
                .ConfigureAwait(false);

            var movieDetails = _mapper.Map<MovieDetailsResponseModel>(movieDetail);

            return movieDetails;
        }
        public async Task<IEnumerable<MovieDisplayResponseModel>> GetHotMovies()
        {
            var hotMovies = await _unitOfWork.EflmMovieRepository.GetHotMovies().ConfigureAwait(false);
            var result = _mapper.Map<IEnumerable<MovieDisplayResponseModel>>(hotMovies);
            return result;
        }
        public async Task<IEnumerable<MovieDisplayResponseModel>> GetNewMovieByTypeMovie(TypeMovie typeMovie)
        {
            var hotMovies = await _unitOfWork.EflmMovieRepository.GetNewMovieByTypeMovie(typeMovie).ConfigureAwait(false);
            var result = _mapper.Map<IEnumerable<MovieDisplayResponseModel>>(hotMovies);
            return result;
        }

        public async Task<AdminListMovieResponseModel> GetMovieList(int currentPage)
        {
            if (currentPage <= 0)
            {
                currentPage = 1;
            }

            var totalItems = await _unitOfWork.EflmMovieRepository.Count();

            var paging = new Pagination(totalItems, currentPage);

            var data = await _unitOfWork.EflmMovieRepository
                .GetMoviePaging(paging.CurrentPage - 1);

            if (!data.Any())
            {
                return null;
            }

            var mappedData = _mapper.Map<List<AdminMovieResponseModel>>(data);

            return new AdminListMovieResponseModel(mappedData, paging);
        }

        public async Task<IEnumerable<MovieEpisodeResponseModel>> GetMovieResponseById(Guid movieId)
        {
            try
            {
                var episodeList = await _unitOfWork.EflmMovieEpisodeRepository
                    .FindMultiple(x => x.MovieId == movieId)
                    .ConfigureAwait(false);

                var mappedData = _mapper.Map<IEnumerable<MovieEpisodeResponseModel>>(episodeList);

                return mappedData;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }
            return Enumerable.Empty<MovieEpisodeResponseModel>();
        }

        public async Task<Guid> AddFilmEpisode(Guid filmId, int episodeNum)
        {
            try
            {
                var movieEpisode = new EflmMovieEpisode(Guid.Empty, filmId, string.Empty, episodeNum);
                await _unitOfWork.EflmMovieEpisodeRepository.AddAsync(movieEpisode);
                await _unitOfWork.CommitAsync().ConfigureAwait(false);
                return movieEpisode.MovieEpisodeId;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }
            return Guid.Empty;
        }
    }
}
