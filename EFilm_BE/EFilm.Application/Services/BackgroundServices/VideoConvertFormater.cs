﻿namespace EFilm.Application.Services.BackgroundServices
{
    public class VideoConvertFormater
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public string VideoBitrate { get; set; }
        public string AudioBitrate { get; set; }

        public VideoConvertFormater(int width, int height, string videoBitrate, string audioBitrate)
        {
            Width = width;
            Height = height;
            VideoBitrate = videoBitrate;
            AudioBitrate = audioBitrate;
        }
    }
}
