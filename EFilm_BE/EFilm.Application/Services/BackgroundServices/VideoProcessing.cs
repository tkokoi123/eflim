﻿using EFilm.Application.Services.Interfaces;
using EFilm.Data.EF;
using EFilm.Data.S3Obj;
using EFilm.Infrastructure.Configurations;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Text;
using System.Threading.Channels;
using static EFilm.Infrastructure.Enums.SystemEnum;

namespace EFilm.Application.Services.BackgroundServices
{
    public class VideoProcessing : BackgroundService
    {
        private readonly IWebHostEnvironment _env;
        private readonly ChannelReader<VideoProcessingMessage> _channelReader;
        private readonly ILogger<VideoProcessing> _logger;
        private readonly IS3StorageService _storage;
        private readonly IServiceProvider _serviceProvider;

        public VideoProcessing(IWebHostEnvironment env, Channel<VideoProcessingMessage> channel,
            ILogger<VideoProcessing> logger, IS3StorageService storage, IServiceProvider serviceProvider)
        {
            _env = env;
            _channelReader = channel.Reader;
            _logger = logger;
            _storage = storage;
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// Get width and height of video
        /// </summary>
        /// <param name="input"></param>
        /// <returns>Dictionary<string, string></returns>
        private Dictionary<string, string> GetVideoInfo(string input)
        {
            Dictionary<string, string> videoInfo = new Dictionary<string, string>();
            var data = input.Split("\n");
            foreach (var kv in from line in data
                               where line.Contains("=")
                               let kv = line.Split("=")
                               select kv)
            {
                videoInfo.Add(kv[0], kv[1]);
            }

            return videoInfo;
        }


        /// <summary>
        /// Generate args for ffmpeg following by height and width of video
        /// </summary>
        /// <param name="videoInfo"></param>
        /// <param name="message"></param>
        /// <returns>string</returns>
        private string GenerateArgString(Dictionary<string, string> videoInfo, VideoProcessingMessage message)
        {
            //Key: width of wideo
            Dictionary<int, VideoConvertFormater> videoResolution = new Dictionary<int, VideoConvertFormater>()
                        {
                            { 1920, new VideoConvertFormater(1920, 1080, "6000k", "4000k") },
                            { 1280, new VideoConvertFormater(1280, 720, "3000k", "2000k") },
                            { 640, new VideoConvertFormater(640, 480, "1500k", "1000k") },
                            { 480, new VideoConvertFormater(480, 360, "600k", "500k") }
                        };

            StringBuilder downscaleArgs = new StringBuilder();

            int videoWidth = Convert.ToInt32(videoInfo["width"]);
            //Args: using cuda to increase speed, max bitrate 8M, fps 60, codex h264_nvenc, audio codex aac, sample rate 44100
            downscaleArgs
                .Append($"-hwaccel cuda -i {message.InputPath} -b:v 8M -g 60 ")
                .Append("-maxrate 8M -c:v h264_nvenc -crf 22 -c:a aac -ar 44100 ");

            if (videoResolution.ContainsKey(videoWidth))
            {
                var data = videoResolution.Where(x => x.Key <= videoWidth)
                    .ToImmutableDictionary();

                var dataCount = data.Count;
                //Map stream with filter
                for (int i = 0; i < dataCount; i++)
                {
                    downscaleArgs.Append("-map 0:v:0 -map 0:a:0 ");
                }

                //Filter config
                for (int i = 0; i < dataCount; i++)
                {
                    var videoConvertFormat = data.ElementAt(i).Value;
                    downscaleArgs
                        .Append($"-filter:v:{i} scale=w={videoConvertFormat.Width}:h={videoConvertFormat.Height} ")
                        .Append($"-maxrate:v:{i} {videoConvertFormat.VideoBitrate} -b:a:{i} {videoConvertFormat.AudioBitrate} ");
                }

                downscaleArgs.Append("-var_stream_map \"");
                for (int i = 0; i < dataCount; i++)
                {
                    var videoConvertFormat = data.ElementAt(i).Value;
                    //Output for each height video
                    downscaleArgs.Append($"v:{i},a:{i},name:{videoConvertFormat.Height}p ");
                }

                downscaleArgs
                    .Append("\" ")
                    .Append("-preset fast -hls_list_size 0 ")
                    .Append("-f hls -hls_time 3 ")
                    .Append("-hls_flags independent_segments -hls_segment_size 2M ")
                    .Append("-master_pl_name \"master.m3u8\" ")
                    .Append($"-y {Path.Combine(message.OutputPath, "output_%v/output.m3u8")}");
            }

            return downscaleArgs.ToString() ?? string.Empty;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (await _channelReader.WaitToReadAsync(stoppingToken))
            {
                VideoProcessingMessage? message = null;
                try
                {
                    message = await _channelReader.ReadAsync(stoppingToken);

                    if (message is null)
                    {
                        _logger.LogError("Does not have message");
                        return;
                    }


                    using (var scope = _serviceProvider.CreateScope())
                    {
                        var ctx = scope.ServiceProvider.GetRequiredService<AppDbContext>();

                        var processing = await ctx.EflmProcessing
                            .FirstOrDefaultAsync(x => x.ProcessingId == Guid.Parse(message.ProcessId))
                            .ConfigureAwait(false);

                        if (processing is null)
                        {
                            _logger.LogError("Film not exist");
                            return;
                        }

                        processing.Status = (short)ProcessStatus.Executing;
                        processing.StartTime = TimeOnly.FromDateTime(DateTime.UtcNow);
                        processing.Message = "Processing";

                        await ctx.SaveChangesAsync(stoppingToken)
                            .ConfigureAwait(false);
                    }

                    //var data = await _eflmProcessingService.GetProcessingById(Guid.Parse(message.ProcessId));


                    //if (data is null)
                    //{
                    //    _logger.LogError("Film not exist");
                    //    return;
                    //}

                    //data.Status = (short)ProcessStatus.Executing;
                    //data.StartTime = TimeOnly.FromDateTime(DateTime.UtcNow);
                    //data.Message = "Processing";

                    //await _eflmProcessingService.UpdateStatusProcesssing(data)
                    //    .ConfigureAwait(false);

                    //Process1: Get video info
                    //Get width and height of video
                    string args = $"-v error -select_streams v:0 -show_entries stream=width,height -of default=noprint_wrappers=1 \"{message.InputPath}\"";
                    ProcessStartInfo ProcessGetVideoInfo = new ProcessStartInfo
                    {
                        FileName = Path.Combine(_env.WebRootPath, "ffmpeg", "ffprobe.exe"),
                        WorkingDirectory = Path.Combine(_env.WebRootPath),
                        Arguments = args,
                        CreateNoWindow = true,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                    };


                    string convertVideoArgs = string.Empty;
                    using (var process = new Process { StartInfo = ProcessGetVideoInfo })
                    {
                        process.Start();
                        string output = await process.StandardOutput.ReadToEndAsync();
                        string err = await process.StandardError.ReadToEndAsync();

                        if (!string.IsNullOrEmpty(err))
                        {
                            return;
                        }
                        process.WaitForExit();
                        if (process.ExitCode != 0)
                        {
                            process.Kill();
                        }

                        Dictionary<string, string> videoInfo = GetVideoInfo(output);

                        convertVideoArgs = GenerateArgString(videoInfo, message);
                    }

                    if (string.IsNullOrEmpty(convertVideoArgs))
                    {
                        return;
                    }

                    //Process2: Convert video to hls, downscale video
                    ProcessStartInfo convertProcessInfo = new ProcessStartInfo
                    {
                        FileName = Path.Combine(_env.WebRootPath, "ffmpeg", "ffmpeg.exe"),
                        WorkingDirectory = Path.Combine(_env.WebRootPath),
                        Arguments = convertVideoArgs,
                        CreateNoWindow = true,
                        UseShellExecute = false,
                    };

                    using (var process = new Process { StartInfo = convertProcessInfo })
                    {
                        Stopwatch stopWatch = new Stopwatch();
                        process.Start();
                        stopWatch.Start();

                        process.WaitForExit();
                        if (process.ExitCode != 0)
                        {
                            process.Kill();
                        }
                        stopWatch.Stop();

                        await _storage.UploadFolderAsync(new S3RquestDirectory
                        {
                            BucketName = AppSettings.ObjectStorageBucket,
                            Prefix = $"efilm/processed_{message.FilmId}",
                            Directory = message.OutputPath
                        }).ConfigureAwait(false);

                        TimeSpan ts = stopWatch.Elapsed;
                        string elapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                                            ts.Hours, ts.Minutes, ts.Seconds,
                                            ts.Milliseconds / 10);
                        await Console.Out.WriteLineAsync("RunTime " + elapsedTime);
                        //Todo: Change status video to processed = 1
                        //Add runtime process to db
                        //data.Status = (short)ProcessStatus.Done;
                        //data.FinishTime = TimeOnly.FromDateTime(DateTime.UtcNow);
                        //data.Message = "Done";

                        //await _eflmProcessingService.UpdateStatusProcesssing(data)
                        //    .ConfigureAwait(false);

                        using (var scope = _serviceProvider.CreateScope())
                        {
                            var ctx = scope.ServiceProvider.GetRequiredService<AppDbContext>();

                            var processing = await ctx.EflmProcessing
                                .FirstOrDefaultAsync(x => x.ProcessingId == Guid.Parse(message.ProcessId))
                                .ConfigureAwait(false);

                            var episode = await ctx.EflmMovieEpisode
                                .FirstOrDefaultAsync(x => x.MovieEpisodeId == Guid.Parse(message.FilmId))
                                .ConfigureAwait(false);

                            if (processing is null)
                            {
                                _logger.LogError("Film not exist");
                                return;
                            }
                            if (episode != null)
                            {
                                episode.ObjectUrl = $"/efilm/{message.FilmId}";
                            }

                            processing.Status = (short)ProcessStatus.Done;
                            processing.FinishTime = TimeOnly.FromDateTime(DateTime.UtcNow);
                            processing.Message = "Done";

                            await ctx.SaveChangesAsync(stoppingToken)
                                .ConfigureAwait(false);
                        }
                    }

                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Error when processing video!");
                }
                finally
                {
                    if (message is not null)
                    {
                        if (File.Exists(message.InputPath))
                        {
                            File.Delete(message.InputPath);
                        }

                        if (Directory.Exists(message.OutputPath))
                        {
                            Directory.Delete(message.OutputPath, true);
                        }
                    }

                }

            }

        }
    }
}
