﻿namespace EFilm.Application.Services.BackgroundServices
{
    public class VideoProcessingMessage
    {
        public string FileId { get; set; } = string.Empty;
        public string FilmId { get; set; } = string.Empty;
        public string ProcessId { get; set; } = string.Empty;
        public string InputPath { get; set; } = string.Empty;
        public string OutputPath { get; set; } = string.Empty;
    }
}