﻿using AutoMapper;
using EFilm.Application.Services.Interfaces;
using EFilm.Data.EF;
using EFilm.Data.Entities;
using EFilm.Infrastructure.Configurations;
using Microsoft.Extensions.Logging;

namespace EFilm.Application.Services
{
    public class EflmTokenService : IEflmTokenService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<EflmUserService> _logger;
        private readonly IMapper _mapper;

        public EflmTokenService(IUnitOfWork unitOfWork, ILogger<EflmUserService> logger, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<bool> SaveRefeshTokenDb(Guid userId, string refreshToken, DateTime expiredTime)
        {
            try
            {
                //Get token by userid
                var token = await GetTokenInfo(userId)
                    .ConfigureAwait(false);

                if (token is not null)
                {
                    //Delete token
                    _unitOfWork.EflmTokenRepository
                        .Delete(token);

                    //Add new token
                    await _unitOfWork.EflmTokenRepository
                        .AddAsync(new EflmToken(userId,
                        refreshToken,
                        DateTime.UtcNow.AddDays(AppSettings.RefreshTokenExpriedTime)))
                        .ConfigureAwait(false);
                }

                await _unitOfWork.CommitAsync()
                    .ConfigureAwait(false);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return false;
        }

        public async Task<EflmToken> GetTokenInfo(Guid userId)
        {
            try
            {
                //Get token by userid
                var token = await _unitOfWork.EflmTokenRepository
                    .FindObject(x => x.UserId == userId)
                    .ConfigureAwait(false);

                return token;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            return null;
        }
    }
}
