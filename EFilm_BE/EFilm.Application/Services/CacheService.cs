﻿using Microsoft.Extensions.Caching.Distributed;
using EFilm.Application.Services.Interfaces;
using System.Text.Json;

namespace EFilm.Application.Services
{
    public class CacheService : ICacheService
    {
        private readonly IDistributedCache _distributedCache;

        public CacheService(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }
        //Get object from cached by key and canellationToken
        public async Task<T?> GetAsync<T>(string key, CancellationToken cancellationToken = default) where T : class
        {
            string? cacheValue = await _distributedCache.GetStringAsync(key, cancellationToken)
                .ConfigureAwait(false);

            if (cacheValue is null)
            {
                return null;
            }

            T? value = JsonSerializer.Deserialize<T>(cacheValue);

            return value;
        }
        //remove cached by key and canellationToken
        public async Task RemoveAsync(string key, CancellationToken cancellationToken = default)
        {
            await _distributedCache.RemoveAsync(key, cancellationToken)
                .ConfigureAwait(false);
        }

        //Set cached
        public async Task SetAsync<T>(string key, T value, DistributedCacheEntryOptions options, CancellationToken cancellationToken = default) where T : class
        {
            string cacheValue = JsonSerializer.Serialize<T>(value);

            await _distributedCache.SetStringAsync(key, cacheValue, options, cancellationToken)
                .ConfigureAwait(false);
        }
    }
}
