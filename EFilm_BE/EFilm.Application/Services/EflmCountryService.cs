﻿using AutoMapper;
using EFilm.Application.Services.Interfaces;
using EFilm.Data.Dtos.Responses;
using EFilm.Data.EF;
using EFilm.Data.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFilm.Application.Services
{
    public class EflmCountryService : IEflmCountryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<EflmCountryService> _logger;
        private readonly IMapper _mapper;

        public EflmCountryService(IUnitOfWork unitOfWork, ILogger<EflmCountryService> logger, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
        }
        public async Task<IEnumerable<CountryResponseModel>> GetCountrys()
        {
            var countrys =await _unitOfWork.EflmCountryRepository.All().ConfigureAwait(false);
            var result = _mapper.Map<IEnumerable<CountryResponseModel>>(countrys);
            return result;
        }
    }
}
