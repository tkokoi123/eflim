﻿using AutoMapper;
using EFilm.Application.Services.Interfaces;
using EFilm.Data.Dtos.Requests;
using EFilm.Data.EF;
using EFilm.Data.Entities;
using EFilm.Infrastructure.Helpers;
using Microsoft.Extensions.Logging;
using static EFilm.Infrastructure.Enums.SystemEnum;

namespace EFilm.Application.Services
{
    public class EflmUserService : IEflmUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<EflmUserService> _logger;
        private readonly IMapper _mapper;

        public EflmUserService(IUnitOfWork unitOfWork, ILogger<EflmUserService> logger, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<EflmUser?> UserLogin(UserLoginRequestModel rqUser)
        {
            try
            {
                var user = await _unitOfWork.EflmUserRepository
                    .FindObject(x => x.Username.Equals(rqUser.Username)
                                && x.Status == (short)UserStatus.Activated
                                && !x.IsDelete)
                    .ConfigureAwait(false);

                if (user is not null && HashingHelper.VerifyPassword(rqUser.Password, user.Password))
                {
                    return user;
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return null;
        }

        public async Task<bool> UserRegister(UserRegisterRequestModel rqUser)
        {
            try
            {
                var userEntity = _mapper.Map<EflmUser>(rqUser);
                userEntity.Password = HashingHelper.EncryptPassword(rqUser.Password);
                userEntity.Mobile = "0123456789";

                await _unitOfWork.EflmUserRepository
                    .AddAsync(userEntity)
                    .ConfigureAwait(false);

                await _unitOfWork.CommitAsync()
                    .ConfigureAwait(false);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return false;
        }

        public async Task<EflmUser?> FindUserByUsername(string? username)
        {
            try
            {
                var user = await _unitOfWork.EflmUserRepository
                    .FindObject(x => x.Username.Equals(username)
                                && x.Status == (short)UserStatus.Activated
                                && !x.IsDelete)
                    .ConfigureAwait(false);

                return user;

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return null;
        }
    }
}
