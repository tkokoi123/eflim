﻿using EFilm.Application.Services.Interfaces;
using EFilm.Data.EF;
using EFilm.Data.Entities;
using Microsoft.Extensions.Logging;

namespace EFilm.Application.Services
{
    public class EflmProcessingService : IEflmProcessingService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogger<EflmProcessingService> _logger;

        public EflmProcessingService(IUnitOfWork unitOfWork, ILogger<EflmProcessingService> logger)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<bool> UpdateStatusProcesssing(EflmProcessing eflmProcessing)
        {
            try
            {
                var process = await _unitOfWork.EflmProcessingRepository
                                .FindObject(x => x.ProcessingId == eflmProcessing.ProcessingId)
                                .ConfigureAwait(false);

                if (process is null)
                {
                    return false;
                }

                process.Status = eflmProcessing.Status;
                process.UpdateDate = DateTime.UtcNow;
                process.Message = eflmProcessing.Message;
                process.StartTime = eflmProcessing.StartTime;
                process.FinishTime = eflmProcessing.FinishTime;

                await _unitOfWork.CommitAsync()
                    .ConfigureAwait(false);
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);

            }

            return false;
        }

        public async Task<EflmProcessing> GetProcessingById(Guid processId)
        {
            try
            {
                var data = await _unitOfWork.EflmProcessingRepository
                    .FindObject(x => x.ProcessingId == processId)
                    .ConfigureAwait(false);

                return data;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }
            return null;
        }

        public async Task<EflmProcessing> AddProcessing(EflmProcessing eflmProcessing)
        {
            try
            {
                if (eflmProcessing is null)
                {
                    return null;
                }

                await _unitOfWork.EflmProcessingRepository
               .AddAsync(eflmProcessing)
               .ConfigureAwait(false);

                await _unitOfWork.CommitAsync()
                    .ConfigureAwait(false);

                return eflmProcessing;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }

            return null;
        }


    }
}
