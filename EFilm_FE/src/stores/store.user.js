import { defineStore } from 'pinia'
import { reactive } from 'vue'
import { register as registerService } from '@/helpers/api/user.service.js'

export const useUserStore = defineStore('userInfo', () => {
    const state = reactive({
        users: {},
        user: {}
    })

    const userRegister = async (userRq) => {
        try {
            const response = await registerService(userRq)

            if (response.status === 200) {
                console.log(response)
            }
        } catch (error) {
            console.log(error)
        }
    }

    const getUserByUserName = async (username) => {
        console.log(username);
    }

    const updateUserInfo = async (userRq) => {
        console.log(userRq);
    }

    return { state, userRegister, getUserByUserName, updateUserInfo }
})
