import { defineStore } from 'pinia'
import { reactive } from 'vue'
import { login as loginService, refreshToken as refreshTokenService } from '@/helpers/api/user.service.js'
import jwt_decode from 'jwt-decode'
import router from '@/router'

const DEFAULT_ROLE_KEY = 'http://schemas.microsoft.com/ws/2008/06/identity/claims/role'

const checkLocalStorage = (key) => {
    const data = localStorage.getItem(key)
    return data !== null && data !== undefined ? data.trim() : null
}

export const useUserIdentity = defineStore('userIdentity', () => {
    const accessToken = checkLocalStorage('accessToken')
    const refreshToken = checkLocalStorage('refreshToken')

    const state = reactive({
        refreshToken: refreshToken != null ? refreshToken : '',
        accessToken: accessToken != null ? accessToken : '',
        username: accessToken != null ? jwt_decode(accessToken).name : '',
        role: accessToken != null ? jwt_decode(accessToken)[DEFAULT_ROLE_KEY] : ''
    })

    const login = async (username, password) => {
        try {
            const responseData = await loginService({ username, password })

            if (responseData.code === 200) {
                //#region Add to state
                state.refreshToken = responseData.response.data.refreshToken
                state.accessToken = responseData.response.data.accessToken
                state.username = username
                state.role = jwt_decode(responseData.response.data.accessToken)[DEFAULT_ROLE_KEY]
                state.isAuthen = true
                //#endregion

                //#region Add to localStorage
                localStorage.setItem('refreshToken', state.refreshToken)
                localStorage.setItem('accessToken', state.accessToken)
                localStorage.setItem('username', state.username)
                localStorage.setItem('role', state.role)
                //#endregion

                switch (state.role) {
                    case 'Admin':
                        router.push('/admin')
                        break

                    case 'User':
                        router.push('/')
                        break

                    default:
                        router.push('/')
                        break
                }
            }
        } catch (error) {
            console.log(error)
        }
    }

    const getNewAccessToken = async (tokenRequest) => {
        try {
            const responseData = refreshTokenService(tokenRequest);

            if (responseData.code === 200) {
                //#region Add to state
                state.refreshToken = responseData.response.data.refreshToken;
                state.accessToken = responseData.response.data.accessToken;
                state.username = jwt_decode(responseData.response.data.accessToken).name;
                state.role = jwt_decode(responseData.response.data.accessToken)[DEFAULT_ROLE_KEY];
                state.isAuthen = true;
                //#endregion

                //#region Add to localStorage
                localStorage.setItem('refreshToken', state.refreshToken);
                localStorage.setItem('accessToken', state.accessToken);
                localStorage.setItem('username', state.username);
                localStorage.setItem('role', state.role);
                //#endregion
            }
        } catch (error) {
            console.log(error);
        }
    }

    const isAuthenticate = () => {
        if (state.refreshToken.length > 0 && state.accessToken.length > 0) {
            return true
        }
        return false
    }

    return { state, login, isAuthenticate, getNewAccessToken }
})
