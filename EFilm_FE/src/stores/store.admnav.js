import { defineStore } from 'pinia'
import { reactive } from 'vue'

export const adminNavState = defineStore('admin-NavState', () => {
    const DEFAULT_KEY = '/admin/dashboard'

    const state = reactive({
        selectedKeys: [DEFAULT_KEY]
    })

    const setSelectedKey = (key) => {
        state.selectedKeys = [key.path]
    }

    return { state, setSelectedKey }
});
