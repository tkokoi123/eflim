import { defineStore } from 'pinia';
import { reactive } from 'vue';
import axiosInstance from '@/helpers/api/axiosInstance';
import {
    seachFilm,
    getListFilmAdmin,
    getFilmDetail,
    getAllCountries,
    getAllGenres,
    getAllActors,
    getAllDirectors,
    getMovieEpisodeByMovieId
} from '@/helpers/api/film.service';

export const useVideoStore = defineStore('videoStore', () => {
    const state = reactive({
        formData: null,
        status: '',
        percent: 0,
    });

    const uploadFile = async (formData) => {

        try {
            console.log("upload");
            state.formData = formData;
            const response = await axiosInstance.post("/admin/upload-video", formData, {
                headers: {
                    'Content-Type': "multipart/form-data",
                }
            })

            if (response === 200) {
                state.status = 'done';
                console.log("wtf");
                return true;
            }

            return false;
        } catch (error) {
            console.log(error);
        }
        return false;
    }

    const searchFilmOmdb = async (filmName) => {
        try {
            const response = await seachFilm(filmName);

            return response;
        } catch (error) {
            console.log(error);
        }
    }

    const setStatus = (status) => {
        state.status = status;
    }

    const loadListFilm = async (currentPage) => {
        try {
            const response = await getListFilmAdmin(currentPage);

            return response;
        } catch (error) {
            console.log(error);

        }
    }

    const getFilmDetails = async (filmId) => {
        try {
            const response = await getFilmDetail(filmId);

            return response;
        } catch (error) {
            console.log(error);

        }
    }

    const getAllCountryList = async () => {
        try {
            const response = await getAllCountries();
            return response;
        } catch (error) {
            console.log(error);

        }
    }

    const getAllGenresList = async () => {
        try {
            const response = await getAllGenres();
            return response;
        } catch (error) {
            console.log(error);
        }
    }

    const getAllActorList = async () => {
        try {
            const response = await getAllActors();
            return response;
        } catch (error) {
            console.log(error);
        }
    }

    const getAllDirectorList = async () => {
        try {
            const response = await getAllDirectors();
            return response;
        } catch (error) {
            console.log(error);
        }
    }

    const getMovieEpisodeById = async (movieId) => {
        try {
            const response = await getMovieEpisodeByMovieId(movieId);
            return response;
        } catch (error) {
            console.log(error);
        }
    }

    return {
        state,
        uploadFile,
        setStatus,
        searchFilmOmdb,
        loadListFilm,
        getFilmDetails,
        getAllCountryList,
        getAllGenresList,
        getAllActorList,
        getAllDirectorList,
        getMovieEpisodeById
    }
});
