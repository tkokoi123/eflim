import 'bootstrap/dist/css/bootstrap.min.css'
import 'ant-design-vue/dist/antd.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import Antd from 'ant-design-vue'
import App from './App.vue'
import router from './router/index.js'
import * as VueRouter from 'vue-router';

const app = createApp(App)

app.use(VueRouter);
app.use(createPinia())
app.use(router)
app.use(Antd)

// app.config.errorHandler = (err) => {
//     router.push({path: '/error', params: {code: err}})
// }

app.mount('#app')

import 'bootstrap/dist/js/bootstrap.js'
