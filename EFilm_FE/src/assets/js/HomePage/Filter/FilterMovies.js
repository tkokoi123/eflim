import { reactive } from 'vue'
const defaultGuid = '00000000-0000-0000-0000-000000000000'
const FilterRequestModelBegin = (type, key) => {
    var FilterRequestModel = reactive({
        genreId: defaultGuid,
        countryId: defaultGuid,
        releaseYear: -1,
        TypeSubtitle: 0,
        TypeMovie: 0,
    })
    switch (type) {
        case 'type-movie':
            FilterRequestModel.TypeMovie = parseInt(key);
            break;
        case 'genre':
            FilterRequestModel.genreId = key;
            break;
        case 'country':
            FilterRequestModel.countryId = key;
            break;
        case 'release-year':
            FilterRequestModel.releaseYear = parseInt(key);
            break;
        default:
            break
    }
    return FilterRequestModel
}
export {FilterRequestModelBegin};
