import ROLES from '@/helpers/constant/role.constant'

const admin = [
    {
        path: '/admin',
        name: 'admin',
        redirect: { path: '/admin/dashboard' },
        component: () => import('@/pages/AdminPage.vue'),
        children: [
            {
                path: 'dashboard',
                name: 'admin-dashboard',
                component: () => import('@/views/adminviews/DashboardView.vue'),
                meta: {
                    isAuthen: true,
                    roleRequire: ROLES.ADMIN
                }
            },
            {
                path: 'monitoring',
                name: 'admin-monitoring',
                component: () => import('@/views/adminviews/MonitoringView.vue'),
                meta: {
                    isAuthen: true,
                    roleRequire: ROLES.ADMIN
                }
            },
            {
                path: 'film-manager',
                name: 'admin-manager',
                component: () => import('@/views/adminviews/FilmManagerView.vue'),
                meta: {
                    isAuthen: true,
                    roleRequire: ROLES.ADMIN
                }
            }, 
            {
                path: 'film-upload',
                name: 'admin-upload',
                component: () => import('@/views/adminviews/AddFilmView.vue'),
                meta: {
                    isAuthen: true,
                    roleRequire: ROLES.ADMIN
                }
            }
        ]
    }
]

export default admin
