import { createRouter, createWebHistory } from 'vue-router'
import admin from './admin/index.js'
import publicPage from './public/index.js'
import { adminNavState } from '@/stores/store.admnav.js'
import { useUserIdentity } from '@/stores/store.auth.js'
import ROLES from '@/helpers/constant/role.constant'

const routes = [...admin, ...publicPage]

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: routes
})

const exceptRoutes = ['/account/login', '/account/register']

router.beforeEach((to) => {
    const userAuth = useUserIdentity()

    if (exceptRoutes.includes(to.fullPath) && userAuth.isAuthenticate()) {
        return { path: '/home' }
    }

    if (to.matched.some((record) => record.meta.isAuthen)) {
        if (!userAuth.isAuthenticate()) {
            return { path: '/account/login' }
        }

        if (
            to.meta.roleRequire !== ROLES.ANONYMOUS &&
            to.meta.roleRequire !== userAuth.state.role
        ) {
            return { path: '/account/login' }
        }
    }

    if (to.matched.some((record) => record.meta.roleRequire)) {
        const adminNav = adminNavState();
        adminNav.setSelectedKey(to);
    }
})

export default router
