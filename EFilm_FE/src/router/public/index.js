import ROLES from '@/helpers/constant/role.constant'

const publicPage = [
    {
        path: '/account',
        name: 'public',
        component: () => import('@/pages/PublicPage.vue'),
        children: [
            {
                path: 'login',
                name: 'public-login',
                component: () => import('@/views/publicviews/LoginView.vue'),
                meta: {
                    isAuthen: false,
                    roleRequire: ROLES.ANONYMOUS
                }
            },
            {
                path: 'register',
                name: 'public-register',
                component: () => import('@/views/publicviews/RegisterView.vue'),
                meta: {
                    isAuthen: false,
                    roleRequire: ROLES.ANONYMOUS
                }
            }
        ]
    },
    {
        path: '/home',
        name: 'home',
        component: () => import('@/pages/PublicPage.vue'),
        children: [
            {
                path: '',
                name: 'home-view',
                component: () => import('@/views/publicviews/HomeView.vue'),
                meta: {
                    isAuthen: false,
                    roleRequire: ROLES.ANONYMOUS
                }
            },
            {
                path: 'filter/:typeMovie/:key',
                name: 'filter-view',
                component: () => import('@/views/publicviews/FilterView.vue'),
                meta: {
                    isAuthen: false,
                    roleRequire: ROLES.ANONYMOUS
                }
            },
            {
                path: 'details/:movieId',
                name: 'details-view',
                component: () => import('@/views/publicviews/DetailsView.vue'),
                meta: {
                    isAuthen: false,
                    roleRequire: ROLES.ANONYMOUS
                }
            }
        ]
    },
    {
        path: '/',
        redirect: { path: '/home' }
    }
    // {
    //     path: '/error',
    //     name: 'error',
    //     component: () => import('@/pages/PublicPage.vue'),
    //     children: [
    //         {
    //             path: '',
    //             name: 'error-view',
    //             component: () =>
    //         }
    //     ]
    // }
]

export default publicPage
