import Hls from 'hls.js'

const playM3u8 = (video, url, instance) => {
    if (Hls.isSupported()) {
        if (instance.hls) instance.hls.destroy()
        const hls = new Hls()
        hls.loadSource(url)
        hls.attachMedia(video)
        instance.hls = hls
        instance.on('destroy', () => hls.destroy())
    } else if (video.canPlayType('application/vnd.apple.mpegurl')) {
        video.src = url
    } else {
        instance.notice.show = 'Unsupported playback format: m3u8'
    }
}

export { playM3u8 }
