import axiosInstance from './axiosInstance'

const seachFilm = async (filmName) => {
    try {
        const response = await axiosInstance.get(`/movie-management/search/${filmName}`);
        return response.data;
    } catch (error) {
        console.log(error)
    }
}

const getListFilmAdmin = async (requestData) => {
    try {
        const response = await axiosInstance.get(`/movie/get-all-paging?currentPage=${requestData}`);
        return response.data;
    } catch (error) {
        console.log(error);
    }
}

const getFilmDetail = async (filmId) => {
    try {
        const response = await axiosInstance.get(`/movie-filter/get-details/${filmId}`);
        return response.data;
    } catch (error) {
        console.log(error);
    }
}

const getAllCountries = async () => {
    try {
        const response = await axiosInstance.get(`/country-management/all`);
        return response.data;
    } catch (error) {
        console.log(error);
    }
}

const getAllGenres = async () => {
    try {
        const response = await axiosInstance.get(`/genre-management/all`);
        return response.data;
    } catch (error) {
        console.log(error);

    }
}

const getAllActors = async () => {
    try {
        const response = await axiosInstance.get(`/actor/all`);
        return response.data;
    } catch (error) {
        console.log(error);

    }
}

const getAllDirectors = async () => {
    try {
        const response = await axiosInstance.get(`/director/all`);
        return response.data;
    } catch (error) {
        console.log(error);

    }
}

const getMovieEpisodeByMovieId = async (movieId) => {
    try {
        const response = await axiosInstance.get(`/episode/get/${movieId}`);
        return response.data;
    } catch (error) {
        console.log(error);

    }
}

export { 
    seachFilm, 
    getListFilmAdmin, 
    getFilmDetail, 
    getAllCountries, 
    getAllGenres,
    getAllActors,
    getAllDirectors,
    getMovieEpisodeByMovieId
}