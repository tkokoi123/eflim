import axiosInstance from './axiosInstance'

const login = async (user) => {
    try {
        const response = await axiosInstance.post('/common/login', user)
        return response.data
    } catch (error) {
        console.log(error)
    }
}

const register = async (user) => {
    try {
        const response = await axiosInstance.post('/common/register', user)
        return response.data
    } catch (error) {
        console.log(error)
    }
}

// tokenRequest{resfreshToken, accessToken}
const refreshToken = async (tokenRequest) => {
    try {
        const response = await axiosInstance.post('/common/refresh-token', tokenRequest);
        return response.data;
    } catch (error) {
        console.log(error);
    }
}

export { login, register, refreshToken }
