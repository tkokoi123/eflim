import axiosInstance from './axiosInstance'
const getAllCountrys = async () => {
    try {
        const response = await axiosInstance.get('/country-management/all')
        return response.data.response.data
    } catch (error) {
        console.log(error)
    }
}

const getAllGenres = async () => {
    try {
        const response = await axiosInstance.get('/genre-management/all')
        return response.data.response.data
    } catch (error) {
        console.log(error)
    }
}

const getHotMovies = async () => {
    try {
        const response = await axiosInstance.get('/movie-filter/get-hot-movies')
        return response.data.response.data
    } catch (error) {
        console.log(error)
    }
}

const getNewMovies = async (typeMovie) => {
    try {
        const response = await axiosInstance.get('/movie-filter/get-new-movies-by-type-movie?' + typeMovie)
        console.log(response);
        return response.data.response.data
    } catch (error) {
        console.log(error)
    }
}

const yearCurrent = async () => {
    var result = []
    let yearCurrent = new Date().getFullYear();
    result.unshift({
        key: yearCurrent - 15,
        value: 'Trước năm '.concat(yearCurrent - 15)
    })
    for (let i = yearCurrent - 14; i <= yearCurrent; i++) {
        result.unshift({
            key: i,
            value: i
        })
    }
    return result;
}

const getMoviesFilter = async (filterRequestModel) => {
    try {
        const response = await axiosInstance.post('/movie-filter', filterRequestModel)
        console.log(response);
        return response.data.response.data
    } catch (error) {
        console.log(error)
    }
}
const getDetailsMovie = async (movieId) => {
    try {
        const response = await axiosInstance.get('/movie-filter/get-details/'+movieId)
        console.log(response)
        return response.data.response.data
    } catch (error) {
        console.log(error)
    }
}
const searchMovieByTag = async (tagName) => {
    try {
        const response = await axiosInstance.get('/movie-filter/search/' + tagName)
        console.log(response)
        return response.data.response.data
    } catch (error) {
        console.log(error)
    }
}
const getSub = async (flimId,subId) => {
    try {
        const response = await axiosInstance.get('/api/v1/film/get-sub/' + flimId+"/"+subId)
        console.log(response)
        return response.data.response.data
    } catch (error) {
        console.log(error)
    }
}

export {
    getAllCountrys,
    getAllGenres,
    getHotMovies,
    getNewMovies,
    yearCurrent,
    getMoviesFilter,
    getDetailsMovie,
    searchMovieByTag,
    getSub,
}
