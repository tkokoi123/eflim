import axios from 'axios';
import { ref } from 'vue';
import { useUserIdentity } from '@/stores/store.auth.js';

const axiosInstance = ref(
    axios.create({
        baseURL: import.meta.env.VITE_APP_API_URL,
        timeout: 300000,
        headers: {
            'Content-Type': 'application/json'
        }
    })
)

axiosInstance.value.interceptors.request.use((config) => {
    const authStore = useUserIdentity();

    if (authStore.isAuthenticate()) {
        config.headers = {
            Authorization: `Bearer ${authStore.state.accessToken}`
        }
    }
    return config;
})

axiosInstance.value.interceptors.response.use((response) => {
    const authStore = useUserIdentity();
    const refreshToken = authStore.state.refreshToken;
    const accessToken = authStore.state.accessToken;

    const responseData = response.data;

    if (responseData.code === 401) {
        console.log('Need get new token');
        authStore.getNewAccessToken({ refreshToken, accessToken });

        const config = response.config;
        config.headers = {
            Authorization: `Bearer ${authStore.state.accessToken}`
        }
        return axiosInstance.value(config);
    }

    return response;
}, err => {
    console.warn(err.response.status);

    if (err.response) {
        console.log(err.response);
    } else {
        return Promise.reject(err);
    }
});

export default axiosInstance.value
