const API_ROUTER = {
    COMMON_LOGIN: '/common/login',
    COMMON_REGISTER: '/common/register',
    COMMON_REFRESH_TOKEN: '/common/refresh-token',
    COMMON_LOGOUT: '/common/logout'
}

export default API_ROUTER;