const ROLES = {
    ADMIN: 'Admin',
    USER: 'User',
    ANONYMOUS: 'Anonymous'
}

export default ROLES
