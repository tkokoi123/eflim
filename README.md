# EFilm

## Run local
### Install npm package

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

## Run docker
### Docker compose start
```sh
docker-compose --env-file ./Config/dev-env.env up -d
```

## Document
### Database diagram
[LucidChart](https://lucid.app/lucidchart/e39e9bbd-ee04-481e-a6a0-ea466c6ea4b6/edit?viewport_loc=-11%2C-11%2C2081%2C1108%2C0_0&invitationId=inv_2959be8e-2442-420a-bac3-998ed7ed67cc)
